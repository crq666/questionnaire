/*
 *  Document   : readyLogin.js
 *  Author     : pixelcave
 *  Description: Custom javascript code used in Login page
 */

var ReadyLogin = function() {

    return {
        init: function() {
            /*
             *  Jquery Validation, Check out more examples and documentation at https://github.com/jzaefferer/jquery-validation
             */

            /* Login form - Initialize Validation */
           
           //表单验证
            $('#form-login').validate({
                errorClass: 'help-block animation-slideUp', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                //验证成功的回调函数
                success: function(e) {
                    e.closest('.form-group').removeClass('has-success has-error');
                    e.closest('.help-block').remove();
                    
                    var ip="http://10.21.30.220:8080/questionnaire";
                    
                    //登录
                    $.ajax({
                    	type:"post",
                    	url:ip+"/login",
                    	data: JSON.stringify({
                    		username: 'admin123456',//$("#login-email").val(),
                    		password: 'admin123456',//$("#login-password").val()
                    	}),
                    	contentType: 'application/json',
                    	dataType: 'json',
                    	success: function(res) {  //res为后端返回的结果
                    		console.log(res);
                    	},
                    	async:true
                    });
                },
                rules: {
                    'login-email': {
                        required: true
//                      email: true
                    },
                    'login-password': {
                        required: true,
                        minlength: 5
                    }
                },
                messages: {
                    'login-email': '请输入您的账号',
                    'login-password': {
                        required: '请输入您的密码',
                        minlength: '您的密码长度至少5位'
                    }
                }
            });
        }
    };
}();