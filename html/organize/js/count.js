(function($) {
    $.fn.numberRock = function(options) {
        var defaults = {
            speed: 24,
            count: 100
        };
        var opts = $.extend({}, defaults, options);

        var div_by = 100,
            count = opts["count"],
            speed = Math.floor(count / div_by),
            sum = 0,
            $display = this,
            run_count = 1,
            int_speed = opts["speed"];
        var num1 = 0;
        var num2 = 0;
        var int = setInterval(function() {
            if(run_count <= div_by && speed != 0) 
            {
                $display.text(sum = speed * run_count);
                run_count++;
            } else if(sum < count) 
            {
                $display.text(++sum);
            } else {
                //num1 = num1 + Math.random()*0.3;              
                num1 = num1 + Math.random()*0.02;   
                var num = Math.floor(num1)
                var flag = parseInt((Math.random()*10+3)*2)
                if(num%flag==0){
                    console.log(num)
                $display.text(sum + num);}
                //clearInterval(int);
            }
        }, int_speed);
    }

})(jQuery);
