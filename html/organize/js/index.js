var tab;

layui.config({
	base: 'js/',
	version:new Date().getTime()
}).use(['element', 'layer'], function() {
		$ = layui.jquery,
		layer = layui.layer,
		$('.admin-side-toggle').on('click', function() {
		var sideWidth = $('#admin-side').width();
		if(sideWidth === 200) {
			$('#admin-body').animate({
				left: '0'
			}); //admin-footer
			$('#admin-footer').animate({
				left: '0'
			});
			$('#admin-side').animate({
				width: '0'
			});
		} else {
			$('#admin-body').animate({
				left: '200px'
			});
			$('#admin-footer').animate({
				left: '200px'
			});
			$('#admin-side').animate({
				width: '200px'
			});
		}
	});
	$('.admin-side-full').on('click', function() {
		var docElm = document.documentElement;
		//W3C  
		if(docElm.requestFullscreen) {
			docElm.requestFullscreen();
		}
		//FireFox  
		else if(docElm.mozRequestFullScreen) {
			docElm.mozRequestFullScreen();
		}
		//Chrome等  
		else if(docElm.webkitRequestFullScreen) {
			docElm.webkitRequestFullScreen();
		}
		//IE11
		else if(elem.msRequestFullscreen) {
			elem.msRequestFullscreen();
		}
		layer.msg('按Esc即可退出全屏');
	});
	
});
function index() {
	var header = '<div class="layui-main"> '+
	   '<div class="admin-login-box">'+
       '<div style="left: 0;width: 300px;line-height: 60px">'+
        '<span style="font-size: 22px;color: #FFFFFF;" >人员管理系统&nbsp;<span class="fa fa-gift"></span></span>'+
        '</div>'+
        '</div>'+
        '<ul class="layui-nav admin-header-item">'+
        '<li class="layui-nav-item">'+
        '<div id="logout"><i class="fa fa-sign-out" aria-hidden="true" style="color:#FFFFFF"></i> 注销</div>'+
        '</li>'+
        '</ul>'+
        '</div>'

	$(".layui-header").html(header);

	var leftnav = '<div class="layui-side layui-bg-black">' +
		' <div class="layui-side-scroll">' +
		
		' <ul class="layui-nav layui-nav-tree"  lay-filter="test">' +
		' <li class="layui-nav-item layui-nav-itemed">' +
		'<a class="" style="background-color:#23262E;"><i class="fa fa-nav fa-cubes"  data-icon="fa-cubes"></i><cite>组织架构管理</cite></a>' +
		' <dl style="margin-bottom:0px">' +
		' <dd style="margin-left:0px;"><a href="personnel.html" >人员库</a></dd>' +
		'</dl>' +
		'</li>' +
		' <li class="layui-nav-item layui-nav-itemed">' +
		'<a class="" style="background-color:#23262E;"><i class="fa fa-nav fa-user" aria-hidden="true"></i><cite>调查小组管理</cite></a>' +
		' <dl>' +
		' <dd style="margin-left:0px;"><a href="teamManage.html">人员组管理</a></dd>' +
		'</dl>' +
		'</li>' +
		'<div style="position:absolute;top:800px;left:20px;">'+
		'<a href="javascript:;" style="color:#ffffff;float:left;"onclick="popweb();">调查管理系统</a>'+
		'<a href="javascript:;" style="color:#ffffff;float:right;margin-left: 10px;"onclick="pop();">联系我们</a>'+
		
		'</div>'+
		'</ul>'+
		'</div></div>'

	$("#admin-side").html(leftnav);

	var footer = '<div class="layui-main">\
					<p>2017 \
						<a href="http://randomdelivery.cn/">randomdelivery.cn/</a> ITObase license\
					</p>\
				</div>';
	$("#admin-footer").html(footer);
}
function pop(){
				layui.use('layer', function(){
  					var layer = layui.layer;
  				  	layer.alert('手机号：18758263422(543422)<br>微信：    F_10086_<br>邮箱：   13677294433@163.com', {
  						skin: 'layui-layer-molv' ,
  						title:'请联系我们',
  						closeBtn: 0
					});
				});
			}

function popweb(){
				window.open("http://qure.zjgsu.edu.cn/ghc/satisfy/login.html");
			}

$(function(){
    index();
    
			
    $('#logout').click(function(){
    	$.ajax({
			type: "get",
			url:ip+"/logout",
			data: {},
			async: true,
			dataType: "",
			contentType: "application/json",
			xhrFields: {
				withCredentials: true
				},
			crossDomain: true,
			success: function(res) {
				window.location.href="login.html";
			},
			Error: function() {
				alert("服务器出错");
			}
		})
    })
})

