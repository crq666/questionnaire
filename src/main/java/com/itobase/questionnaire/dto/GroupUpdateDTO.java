package com.itobase.questionnaire.dto;

import lombok.Data;

/**
 * @author: lphu
 * @create: 2018-10-28 20:22
 * @description:
 */
@Data
public class GroupUpdateDTO
{
    private String surveyGroupId;
    private String oldGroupName;
    private String newGroupName;
}
