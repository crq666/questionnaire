package com.itobase.questionnaire.dto;

import lombok.Data;

/**
 * @author: lphu
 * @create: 2018-11-10 15:36
 * @description:
 */

@Data
public class UpdateTagDTO
{
    private String oldTagName;
    private String newTagName;
}
