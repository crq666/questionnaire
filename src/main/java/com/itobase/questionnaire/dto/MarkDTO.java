package com.itobase.questionnaire.dto;

import lombok.Data;

import java.util.List;

/**
 * @author: lphu
 * @create: 2018-11-11 14:33
 * @description:
 */

@Data
public class MarkDTO
{
    private List<String> tagIds;
    private List<String> userIds;
}
