package com.itobase.questionnaire.dto;

import com.itobase.questionnaire.util.Convert;
import lombok.Data;

/**
 * @author: lphu
 * @create: 2018-10-28 19:38
 * @description:
 */
@Data
public class SurveyGroupUpdateDTO
{
    private String surveyGroupId;
    private String surveyGroupName;
}
