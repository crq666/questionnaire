package com.itobase.questionnaire.dto;

import lombok.Data;

/**
 * @author: lphu
 * @create: 2018-09-24 16:00
 * @description:
 */

@Data
public class SurveyGroupDTO
{
    private String surveyGroupId;
    private String group;
}
