package com.itobase.questionnaire.dto;

import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * @author: lphu
 * @create: 2018-09-07 14:24
 * @description:
 */
@Data
public class UserDTO {
    private String surveyGroupId;
    private String group;
    private List<String> userIds;
}
