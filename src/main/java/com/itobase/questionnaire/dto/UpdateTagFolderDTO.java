package com.itobase.questionnaire.dto;

import lombok.Data;

/**
 * @author: lphu
 * @create: 2018-11-10 15:43
 * @description:
 */
@Data
public class UpdateTagFolderDTO
{
    private String oldTagFolderName;
    private String newTagFolderName;
}
