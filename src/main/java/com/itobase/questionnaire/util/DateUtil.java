package com.itobase.questionnaire.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author cong
 * @date 2018/7/28
 */
public class DateUtil {
    static SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    static SimpleDateFormat format_day = new SimpleDateFormat("yyyy-MM-dd");

    /**
     * 将日期格式化并返回字符串
     *
     * @param date 日期
     * @return 时间字符串
     */
    public static String format(Date date) {

        return format.format(date);
    }

    public static Date parse(String timeStr) throws ParseException {
        return format.parse(timeStr);
    }

    public static String format_day(Date date) {

        return format_day.format(date);
    }

    public static Date parse_day(String timeStr) throws ParseException {
        return format_day.parse(timeStr);
    }

}
