package com.itobase.questionnaire.util;


import java.util.*;

/**
 * @author: lphu
 * @create: 2018-10-02 17:19
 * @description:  list去重工具类
 */

public class ListUtil
{

    public static List removeDuplicateWithOrder(List list) {
        Set set = new HashSet();
        List newList = new ArrayList();
        for (Iterator iter = list.iterator(); iter.hasNext();) {
            Object element = iter.next();
            if (set.add(element))
                newList.add(element);
        }
        list.clear();
        list.addAll(newList);
        return newList;
    }

    public static void main(String[] args) {
        Set set = new HashSet();
        set.add("id");
        if (set.add("id"))
            System.out.println("11");
        System.out.println("22");
    }
}
