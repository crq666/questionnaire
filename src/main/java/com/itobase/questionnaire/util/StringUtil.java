package com.itobase.questionnaire.util;

/**
 * @author: lphu
 * @create: 2018-08-01 14:37
 * @description: 字符串工具类
 */

public class StringUtil {
    public static final boolean isEmpty(String str) {
        return !isNotEmpty(str);
    }

    public static final boolean isNotEmpty(String str) {
        return str != null && str.length() > 0;
    }
}
