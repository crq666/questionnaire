package com.itobase.questionnaire.util;

import org.springframework.beans.BeanUtils;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * @author: lphu
 * @create: 2018-10-08 10:38
 * @description: 数据转化接口
 */
public interface Convert<T,S>
{
    /**
     * 将前者属性拷贝至后者
     * @return
     */
    default S convert()
    {
        Class<?>[] interfaces =  this.getClass().getInterfaces();
        int i = 0;
        for (Class<?> clazz : interfaces)
        {
            //检测到Convert接口后跳出循环
            if (clazz==Convert.class)
            {
                //System.out.println("检测到Convert接口");
                break;
            }
            i++;
        }
        Type type = this.getClass().getGenericInterfaces()[i];//获取类型
        S s = null;
        if (type instanceof ParameterizedType)
        {
            Type[] types = ((ParameterizedType) type).getActualTypeArguments();
            try
            {
                //取得泛型中的后者强制转化为类对象,同时新建对象
                s = (S) ((Class) types[1]).newInstance();//强制转型
                BeanUtils.copyProperties(this,s);//属性赋值
            } catch (InstantiationException e)
            {
                e.printStackTrace();
            } catch (IllegalAccessException e)
            {
                e.printStackTrace();
            }
        }else
        {
            return null;
        }
        return s;
    }

    /**
     * 将后者属性拷贝至前者
     * @param s
     * @return
     */
    default T reconvert(S s)
    {
        BeanUtils.copyProperties(s,this);
        return (T)this;
    }
}