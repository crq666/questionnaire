package com.itobase.questionnaire.util;

/**
 * @author liucong
 * @date 2018/10/12
 */
import java.util.List;

import com.itobase.questionnaire.VO.PageDatas;

/**
 * @Title   PagingUtils.java
 * @Package com.pro.huanbao.common.utils
 * @author  wanpu_ly
 * @dade    2017年11月3日 上午11:10:09
 * @version V1.0
 * 类说明:	分页
 */
public class PagingUtils {

    /**
     * @Title   paging
     * @param   @param page 		当前页
     * @param   @param allDatas		需要分页的数据集
     * @param   @param currentSize	每页数据条数
     * @param   @return
     * @return  PageDatas<E>
     */
    public static <E> PageDatas<E> paging(Integer page, List<E> allDatas , Integer currentSize) {
        try {
            if(allDatas.size() == 0)
                return null;
            if (page == null || page == 0)
                page = 1;
            // 总记录数
            long total = allDatas.size();//21
            // 总页数
            int pages;//2
            // 每页记录数
            if (total < currentSize) {
                pages = 1;
            }else {
                if (total % currentSize == 0) {
                    pages = (int) (total / currentSize);
                } else {
                    pages = (int) (total / currentSize + 1);
                }
            }
            // 当前页数据第一个索引
            int currentPage ;
            if (page == 1) {
                currentPage = 0;
            }else {
                currentPage = (page - 1) * currentSize;//20
            }
            // 当前页数据最后一个索引
            int finalRecord;
            // 当前页数据集
            List<E> datas = null;
            if (currentSize >= total) {
                finalRecord = (int) total;
            }else if (page == pages) {
                finalRecord = (int) total;
            }else {
                finalRecord = currentPage + currentSize;
            }
            if (currentPage > total || finalRecord > total) {
                return new PageDatas<>(total,pages,allDatas);
            }
            if (currentSize >= total) {
                datas = allDatas;
            } else {
                datas = allDatas.subList(currentPage, finalRecord);
            }

            return new PageDatas<>(total, pages, datas);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}

