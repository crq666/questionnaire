package com.itobase.questionnaire.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;

/**
 * @author: lphu
 * @create: 2018-10-18 10:28
 * @description:
 */
@Document(collection = "term")
@Data
public class Term
{
    @Id
    private String id;

    @NotNull
    private String termName;

    private List<Tag> tags;

    private Map<String,Integer> tag_count;
}
