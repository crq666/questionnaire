package com.itobase.questionnaire.model;

import com.itobase.questionnaire.model.auth.User;
import lombok.Data;

/**
 * @author liucong
 * @date 2018/11/7
 */
@Data
public class Admin extends User {
}
