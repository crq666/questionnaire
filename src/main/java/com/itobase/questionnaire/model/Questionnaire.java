package com.itobase.questionnaire.model;

import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * @author cong
 * @date 2018/7/28
 */
@Document
@Data
public class Questionnaire {

    @Id
    private String id;

    /**
     * 问卷名称
     */
    private String name;

    /**
     * 问卷描述
     */
    private String description;

    /**
     * 题目list
     */
    private List<Question> questionList = Collections.emptyList();

    /**
     * 题目存储的路径
     */
    private String xmlStr;


    /**
     * 是否已经被调查关联,true表示已经被关联,false表示未被关联
     */
    private Boolean used;

    /**
     * 问卷是否失效 true为失效，false为有效
     */
    private Boolean expired = false;

    /**
     * 问卷创建时间
     */
    private Date createTime;

}
