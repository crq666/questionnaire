package com.itobase.questionnaire.model;

import lombok.Data;
import org.springframework.data.annotation.Id;

/**
 * @author liucong
 * @date 2018/8/6
 */
@Data
public class MessageCount {

    @Id
    private String id;

    /**
     * 总共发送的信息数量
     */
    private Integer total = 0;

    private String lastSend;


}
