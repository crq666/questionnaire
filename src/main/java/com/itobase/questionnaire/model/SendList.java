package com.itobase.questionnaire.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author: lphu
 * @create: 2018-10-31 19:22
 * @description:
 */

@Document(collection = "send_list")
@Data
public class SendList
{
    @Id
    private String id;

    private String userId;

    private String username;

    private String name;

    private String longPhone;

    private String surveyId;

    private String userTypeId;
}
