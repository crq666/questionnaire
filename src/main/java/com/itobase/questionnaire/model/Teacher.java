package com.itobase.questionnaire.model;

import com.itobase.questionnaire.model.auth.User;
import lombok.Data;

/**
 * @author: lphu
 * @create: 2018-07-30 16:21
 * @description:
 */
@Data
public class Teacher extends User
{
}
