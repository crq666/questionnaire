package com.itobase.questionnaire.model;

import lombok.Data;
import org.springframework.data.annotation.Id;

import java.util.Map;

/**
 * @author liucong
 * @date 2018/9/27
 */
@Data
public class SectionRate {

    @Id
    private String id;

    private String sectionName;

    private Map<String,Float> typeRate;
}
