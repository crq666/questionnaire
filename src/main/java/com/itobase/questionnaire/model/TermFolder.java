package com.itobase.questionnaire.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;

/**
 * @author: lphu
 * @create: 2018-10-18 09:40
 * @description:
 */
@Document(collection = "term_folder")
@Data
public class TermFolder
{
    @Id
    private String id;

    @NotNull
    private String termFolderName;
}