package com.itobase.questionnaire.model;

import lombok.Data;
import org.springframework.data.annotation.Id;

/**
 * @author liucong
 * @date 2018/9/25
 */

@Data
public class ShortUrl {

    @Id
    private String id;

    private String longUrl;

    private String shortUrl;

    private String surveyId;

}
