package com.itobase.questionnaire.model;

import com.itobase.questionnaire.model.EnumList.States;
import com.itobase.questionnaire.model.auth.User;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * @author liucong
 * @date 2018/7/23
 */

@Document
@Data
public class Survey {


    @Id
    private String id;

    /**
     * 调查名称
     */
    @NotNull
    private String title;

    /**
     * 调查描述
     */
    private String description;

    /**
     * 是否公开, true表示匿名,false表示非匿名
     */
    private Boolean privately;

    /**
     * 是否是满意度调查,true表示是满意度调查，false表示不是
    private Boolean satisfy;*/

    /**
     * 调查状态，
     */
    private States state = States.notStarted;

    /**
     * 若不公开,绑定的人员列表
     */
    private List<User> userList = Collections.emptyList();


    /**
     * 所绑定的问卷ID
     */
    private String questionsID;

    /**
     * 调查开始时间
     */
    private String startTime;

    /**
     * 调查结束时间
     */
    private String endTime;

    /**
     * 问卷发起人id
     */
    private String userId;

    /**
     * 发送给问卷填写人的链接
     */
    private String url;

    /*
     调查绑定的人员组
     private String surveyGroup;*/

    /**
     * 调查绑定的调查组的id;
     */
    private String surveyGroupId;

    /**
     * 调查面向的部门，用于后期计算满意度
     */
    private String section;

    /**
     * 本次调查中已发送短信的人员列表
     */
    private List<User> userList_informed = Collections.emptyList();

    /**
     * 调查满意度
     */
    private String contentRate;


    /**
     * 调查创建时间
     */
    private Date createTime;



}