package com.itobase.questionnaire.model;

import com.itobase.questionnaire.model.auth.User;
import lombok.Data;

/**
 * @author: lphu
 * @create: 2018-10-27 10:58
 * @description:
 */
@Data
public class GraduateStudent extends User
{
}
