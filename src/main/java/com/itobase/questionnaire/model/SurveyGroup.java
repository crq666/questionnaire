package com.itobase.questionnaire.model;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author: lphu
 * @create: 2018-08-31 14:31
 * @description:
 */
@Document(collection = "survey_group")
@Data
public class SurveyGroup
{
    @Id
    private String id;
    @NotNull
    private String groupName;
    /**
     * 调查小组中保存用户id
     */
    private Map<String,List<String>> group;

    private String desc;

    @CreatedDate
    private Date createTime = new Date() ;
}
