package com.itobase.questionnaire.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.IndexDirection;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import java.awt.image.DirectColorModel;
import java.util.Collections;
import java.util.Map;

/**
 * @author liucong
 * @date 2018/7/26
 */
@Document
@CompoundIndex(name = "userId_surveyId", def = "{'surveyId':1,'userId':1}")
@Data
public class Submission {

    @Id
    private String id;

    /**
     * 该提交对应的调查的id
     */
    @NotNull
    private String surveyId;

    /**
     * 该提交对应的用户的id;
     */
    private String userId;

    /**
     * 提交对应的人的身份，0是老师，1是学生
     */
    private Integer identity;


    /**
     * 由于一个人会存在一份调查小组的多个类型组里，根据type进行区分。
     * 该map保存<type,satisfaction>构成的map；
     */
    private Map<String,String> outcome = Collections.emptyMap();

    /**
     * 该提交的用户相关信息,ip,浏览器信息;
     * key目前有IP,Browser,
     */
    private Map<String, String> submitInfo = Collections.emptyMap();

    /**
     * 匿名用户提交的唯一标识符；
     */
    private String uuid;

    /**
     * 用户提交时间
     */
    @Indexed(direction = IndexDirection.DESCENDING)
    private String submitTime;

    /**
     * 回答的list,题号和答案,多项选择题用逗号隔开,从1开始；
     */
    private Map<Integer, String> answer = Collections.emptyMap();
}
