package com.itobase.questionnaire.model;


import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Objects;

@Data
@Document
public class SendPool {

    /**
     * 人员id
     */
    @Id
    private String id;

    /**
     * 人员学号/工号
     */
    private String nickName;

    /**
     * 人员姓名
     */
    private String name;


    /**
     * 调查名
     */
    private String survey;

    /**
     * 人员手机号
     */
    private String phoneNumbr;

    /**
     * 状态
     */
    private String status;

    /**
     * 通知次数
     */
    private String number;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SendPool sendPool = (SendPool) o;
        return Objects.equals(phoneNumbr, sendPool.phoneNumbr);
    }

    @Override
    public int hashCode() {
        return Objects.hash(phoneNumbr);
    }
}
