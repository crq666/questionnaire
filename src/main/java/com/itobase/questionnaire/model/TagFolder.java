package com.itobase.questionnaire.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;

/**
 * @author: lphu
 * @create: 2018-10-18 09:26
 * @description:
 */
@Document(collection = "tag_folder")
@Data
public class TagFolder
{
    @Id
    private String id;

    @NotNull
    private String tagFolderName;


}
