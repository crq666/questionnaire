package com.itobase.questionnaire.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;

/**
 * @author: lphu
 * @create: 2018-08-01 13:51
 * @description:
 */

@Document(collection = "tags")
@Data
public class Tag {
    @Id
    private String id;

    @NotNull(message = "标签名称")
    private String tagName;
    /**
     * 父标签id
     */
    @NotNull(message = "标签文件夹Id")
    private String tagFolderId;


}
