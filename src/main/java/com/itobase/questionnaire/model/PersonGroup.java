package com.itobase.questionnaire.model;

import com.itobase.questionnaire.model.auth.User;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

/**
 * @author: lphu
 * @create: 2018-08-31 14:11
 * @description:
 */
@Document(collection = "person_group")
@Data
public class PersonGroup
{
    @Id
    private String id;

    private String groupName;

    private String mark;

}
