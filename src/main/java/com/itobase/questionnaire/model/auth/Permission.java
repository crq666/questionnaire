package com.itobase.questionnaire.model.auth;


import java.io.Serializable;
import java.util.List;

/**
 * @author liucong
 * @date 2018/7/24
 */

public class Permission implements Serializable {


    private Integer id;

    private String permission;


    private List<Role> roles;

    public Permission(Integer id, String permission, List<Role> roles) {
        this.id = id;
        this.permission = permission;
        this.roles = roles;
    }

    public Permission() {
    }


    public Integer getId() {
        return id;
    }

    public String getPermission() {
        return permission;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }
}
