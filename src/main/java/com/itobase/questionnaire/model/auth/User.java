package com.itobase.questionnaire.model.auth;


import com.itobase.questionnaire.model.Tag;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.Objects;


@Document(collection = "users")
@Data
public class User
{

    @Id
    private String id;
    /**
     * username 唯一
     */
    @NotNull(message = "学号不可为空")
    private String username;

    @NotNull(message = "姓名不能为空")
    private String name;

    @NotNull(message = "密码不能为空")
    @Size(min = 6, message = "密码不能少于六位")
    private String password;

    private String gender;

    private String collegeName;
    /**
     * 邮箱
     */
    private String mail;

    private String longPhone;

    private String shortPhone;

    private String userTypeId;

    private String className;

    private String major;

    private String level;

    /**
     * 标签List
     */
    private List<Tag> tag;

    /**
     * 判断是否被禁用
     */
    private Boolean flag = true;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        User user = (User) o;
        return Objects.equals(id, user.id) &&
                Objects.equals(username, user.username) &&
                Objects.equals(name, user.name) &&
                Objects.equals(password, user.password) &&
                Objects.equals(gender, user.gender) &&
                Objects.equals(collegeName, user.collegeName) &&
                Objects.equals(longPhone, user.longPhone) &&
                Objects.equals(shortPhone, user.shortPhone) &&
                Objects.equals(userTypeId, user.userTypeId) &&
                Objects.equals(tag, user.tag) &&
                Objects.equals(flag, user.flag);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, username, name, password, gender, collegeName, longPhone, shortPhone, userTypeId, tag, flag);
    }
}
