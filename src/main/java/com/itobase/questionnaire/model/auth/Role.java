package com.itobase.questionnaire.model.auth;


import java.util.List;

/**
 * @author liucong
 * @date 2018/7/24
 */

public class Role {

    private Integer id; // 编号

    private String role; // 角色标识程序中判断使用,如"admin",这个是唯一的:

    private String description; // 角色描述,UI界面显示使用

    private Boolean available = Boolean.FALSE; // 是否可用,如果不可用将不会添加给用户

    //角色 -- 权限关系：多对多关系;

    private List<Permission> permissions;

    // 用户 - 角色关系定义;

    private List<User> users;// 一个角色对应多个用户


    public Role() {
    }

    public Role(Integer id, String role, String description, Boolean available, List<Permission> permissions, List<User> users) {

        this.id = id;
        this.role = role;
        this.description = description;
        this.available = available;
        this.permissions = permissions;
        this.users = users;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setAvailable(Boolean available) {
        this.available = available;
    }

    public void setPermissions(List<Permission> permissions) {
        this.permissions = permissions;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public Integer getId() {

        return id;
    }


    public String getRole() {
        return role;
    }

    public String getDescription() {
        return description;
    }

    public Boolean getAvailable() {
        return available;
    }

    public List<Permission> getPermissions() {
        return permissions;
    }

    public List<User> getUsers() {
        return users;
    }
}
