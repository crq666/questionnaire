package com.itobase.questionnaire.model.EnumList;

import lombok.Data;

@Data
public class MOItem {

    private String mobile;

    private String srvNo;

    private String msg;

    private String moTime;
}
