package com.itobase.questionnaire.model.EnumList;

/**
 * 题目类型
 *
 * @author cong
 * @date 2018/7/28
 */
public enum QuestionTypes {
    /**
     * 多选题
     */
    Multiple,
    /**
     * 单选题
     */
    Single,

    /**
     * 问答题
     */
    Blanks,


}
