package com.itobase.questionnaire.model.EnumList;

/**
 * 调查的4种状态
 *
 * @author cong
 * @date 2018/7/28
 */
public enum States {
    /**
     * 暂停
     */
    paused,
    /**
     * 未开始
     */
    notStarted,
    /**
     * 运行中
     */
    running,
    /**
     * 已结束
     */
    ended;
}
