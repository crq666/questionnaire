package com.itobase.questionnaire.model;

import com.itobase.questionnaire.model.EnumList.QuestionTypes;
import lombok.Data;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * 问卷中的一个题目
 *
 * @author cong
 * @date 2018/7/28
 */
@Data
public class Question implements Comparable<Question> {

    /**
     * 题目序号
     */
    private Integer orderNumber;

    /**
     * 题目类型enum
     */
    private QuestionTypes type;

    /**
     * 题目是否为必答题,true表示必答
     */
    private Boolean required;

    /**
     * 题目描述
     */
    private String issue;

    /**
     * 选择题的选项
     * 在存储时将选项对应的A`B`C`D也放到该选项的string中
     */
    private List<String> options = Collections.emptyList();


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Question)) return false;
        Question question = (Question) o;
        return Objects.equals(orderNumber, question.orderNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(orderNumber);
    }

    @Override
    public int compareTo(Question o) {
        return this.getOrderNumber().compareTo(o.orderNumber);
    }
}
