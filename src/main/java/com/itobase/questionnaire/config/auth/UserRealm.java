package com.itobase.questionnaire.config.auth;

import com.itobase.questionnaire.model.auth.User;
import com.itobase.questionnaire.model.auth.Permission;
import com.itobase.questionnaire.model.auth.Role;
import com.itobase.questionnaire.service.IUserService;
import com.itobase.questionnaire.service.impl.ShiroUserService;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;


/**
 * @author liucong
 * @date 2018/7/24
 */
public class UserRealm extends AuthorizingRealm {

    @Autowired
    ShiroUserService userService;

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        //获取用户的输入的账号.
        String username = (String) token.getPrincipal();
        System.out.println(token.getCredentials());
        //通过username从数据库中查找 User对象，如果找到，没找到.
        //实际项目中，这里可以根据实际情况做缓存，如果不做，Shiro自己也是有时间间隔机制，2分钟内不会重复执行该方法
        User userInfo = userService.findByUsername(username);
        if (userInfo == null) {
            return null;
        }
        System.out.println(new Date()+"  Login---->>>：userInfo={name=" + userInfo.getName()+";id="+userInfo.getId()+"}");
        return new SimpleAuthenticationInfo(
                userInfo, //用户名
                userInfo.getPassword(), //密码
                getName()  //realm name
        );
    }


    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        System.out.println("权限配置-->MyShiroRealm.doGetAuthorizationInfo()");
        SimpleAuthorizationInfo authorizationInfo = new SimpleAuthorizationInfo();
        User userInfo = (User) principals.getPrimaryPrincipal();
//        for(Role role:userInfo.getRoleList()){
//            authorizationInfo.addRole(role.getRole());
//            for(Permission p:role.getPermissions()){
//                authorizationInfo.addStringPermission(p.getPermission());
//            }
//        }
        return authorizationInfo;
    }


}
