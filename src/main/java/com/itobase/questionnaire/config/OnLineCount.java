package com.itobase.questionnaire.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * @author liucong
 * @date 2018/12/6
 */
@WebListener
public class OnLineCount implements HttpSessionListener {

    private Logger logger = LoggerFactory.getLogger(getClass());

    public int count=0;//记录session的数量
    //监听session的创建,synchronized 防并发bug
    public synchronized void sessionCreated(HttpSessionEvent arg0) {
        count++;
        logger.info("【HttpSessionListener监听器】新增session:当前session数量{}",count);
        arg0.getSession().getServletContext().setAttribute("count", count);
    }
    @Override
    public synchronized void sessionDestroyed(HttpSessionEvent arg0) {//监听session的撤销
        count--;
        logger.info("【HttpSessionListener监听器】注销session:当前session数量{}",count);
        arg0.getSession().getServletContext().setAttribute("count", count);
    }
}

