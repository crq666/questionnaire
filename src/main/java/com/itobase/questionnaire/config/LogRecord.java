package com.itobase.questionnaire.config;

import com.alibaba.fastjson.JSON;


import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LogRecord {

    private Logger logger = LoggerFactory.getLogger(getClass());


    @Before(value = "execution(* com.itobase.questionnaire.service.impl..*.*(..))")
    public void log(JoinPoint joinPoint) {
        String methodName = joinPoint.getSignature().getName();
        logger.warn(String.format("记录管理员操作-----%s", methodName));
    }

}
