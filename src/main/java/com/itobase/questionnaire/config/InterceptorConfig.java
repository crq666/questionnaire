package com.itobase.questionnaire.config;

import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.serializer.ValueFilter;
import com.alibaba.fastjson.support.config.FastJsonConfig;
import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

/**
 * @author liucong
 * @date 2018/9/9
 */

@Configuration
    public class InterceptorConfig extends WebMvcConfigurerAdapter {

        @Autowired
        MyProps myProps;



        @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/ghc/**").addResourceLocations(myProps.getStaticPath());
        super.addResourceHandlers(registry);
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new CorsConfigInterceptor()).addPathPatterns("/**");
    }



    /*@Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        Jackson2ObjectMapperBuilder builder = new Jackson2ObjectMapperBuilder()
                .indentOutput(true);
        MappingJackson2HttpMessageConverter converter=new MappingJackson2HttpMessageConverter();
        converter.setObjectMapper(new JsonObjectMapper());
        converters.add(converter);
        converters.add(new MappingJackson2HttpMessageConverter(builder.build()));
    }*/

    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        FastJsonHttpMessageConverter fastConverter = new FastJsonHttpMessageConverter();
        FastJsonConfig fastJsonConfig = new FastJsonConfig();
        /*fastJsonConfig.setSerializerFeatures(SerializerFeature.QuoteFieldNames,
                SerializerFeature.WriteEnumUsingToString,
                SerializerFeature.WriteMapNullValue,
                SerializerFeature.WriteDateUseDateFormat,
                SerializerFeature.DisableCircularReferenceDetect);*/
        fastJsonConfig.setSerializeFilters((ValueFilter) (o, s, source) -> {
            if (source == null || source.equals("null")) {
                return "";
            }
            return source;
        });
        //处理中文乱码问题
        List<MediaType> fastMediaTypes = new ArrayList<>();
        fastMediaTypes.add(MediaType.APPLICATION_JSON_UTF8);
        fastConverter.setSupportedMediaTypes(fastMediaTypes);
        fastConverter.setFastJsonConfig(fastJsonConfig);

        ByteArrayHttpMessageConverter byteArrayHttpMessageConverter = new ByteArrayHttpMessageConverter();
        byteArrayHttpMessageConverter.setDefaultCharset(Charset.forName("UTF-8"));

        //ByteArrayHttpMessageConverter默认处理请求类型就是APPLICATION_OCTET_STREAM
        List<MediaType> byteMediaTypes = new ArrayList<>();

        byteMediaTypes.add(MediaType.APPLICATION_OCTET_STREAM);
        byteArrayHttpMessageConverter.setSupportedMediaTypes(byteMediaTypes);

        converters.add(byteArrayHttpMessageConverter);
        converters.add(fastConverter);
    }



}

