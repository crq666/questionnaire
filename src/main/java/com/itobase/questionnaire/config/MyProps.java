package com.itobase.questionnaire.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author liucong
 * @date 2018/10/30
 */
@Component
@ConfigurationProperties(prefix="myProps")
@Data
public class MyProps {

    private String staticPath;

    private String ip;

}
