package com.itobase.questionnaire.template;

import com.mongodb.BasicDBList;
import org.codehaus.groovy.transform.tailrec.ReturnStatementToIterationConverter;

import java.util.Collection;

/**
 * @author: lphu
 * @create: 2018-08-08 21:49
 * @description:
 */

public class Constant
{
    public final static String FINISHED= "已完成";

    public final static String TESTING="未完成";

    public final static String UNINFORMED= "未通知";

    public final static String INFORMING = "待通知";

    public final static String INFORMED = "已通知";

    public final static String OTHER_USERS = "补充组";

    public final static String SUCCESS_SEND = "已发送";

    public final static String DEFAULT_SEND = "未发送";

    public final static String GRADUATE_STUDENT_ID = "5b88e7e8dc8b5a1a84a96370";

    public final static String STUID="5b88e7a5dc8b5a1a84a96367";

    public final static String TEACID="5b88e7e8dc8b5a1a84a96368";

    public final static String ADMINID="5b88e7f1dc8b5a1a84a96369";

    public static final Integer teacNum = 3;

    public static final Integer stuNum = 4;

    public static final Integer teacIdentity = 0;

    public static final Integer stuIdentity = 1;



}
