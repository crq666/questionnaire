package com.itobase.questionnaire.template;

import lombok.Data;

import java.util.List;

/**
 * @author: lphu
 * @create: 2018-09-02 13:12
 * @description:
 */
@Data
public class Page<T>
{
    private List<T> data;

    private long totalCount;
}

