package com.itobase.questionnaire.VO;

import lombok.Data;

/**
 * @author liucong
 * @date 2018/10/27
 */
@Data
public class ContentVO {
    /**
     * 该类型对应的人数
     */
    private Integer personCount = 0;

    /**
     * 这些人数对应的满意度和；
     */
    private Float partGrade = 0F;

    public void personCountPlus(){
        this.personCount++;
    }

    public void gradePlus(Float partGrade){
        this.partGrade+=partGrade;
    }

    public ContentVO(Integer personCount, Float partGrade) {
        this.personCount = personCount;
        this.partGrade = partGrade;
    }
}
