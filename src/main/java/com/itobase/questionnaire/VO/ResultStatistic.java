package com.itobase.questionnaire.VO;

import lombok.Data;

/**
 * 统计选择题的各个选项百分比
 *
 * @author liucong
 * @date 2018/8/5
 */


@Data
public class ResultStatistic {
    private String answer;

    private Integer count;

    private String percents;

}
