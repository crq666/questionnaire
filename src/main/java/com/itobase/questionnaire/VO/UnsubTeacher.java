package com.itobase.questionnaire.VO;

import lombok.Data;

/**
 * @author: lphu
 * @create: 2018-12-08 20:13
 * @description:
 */

@Data
public class UnsubTeacher {
    private String username;
    private String name;
    private String phone;
    private String surveyGroupName;
    private String group;
    private String college;
}
