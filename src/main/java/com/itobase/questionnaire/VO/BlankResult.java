package com.itobase.questionnaire.VO;

import lombok.Data;

/**
 * 进行结果统计时，题目类型为填空题时需要返回的vo
 *
 * @author liucong
 * @date 2018/8/5
 */
@Data
public class BlankResult {
    private String id;
    private String time;
    private String answer;

}
