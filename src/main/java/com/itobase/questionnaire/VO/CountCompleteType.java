package com.itobase.questionnaire.VO;

import lombok.Data;

import java.util.Map;

/**
 * @author cong
 * @date 2018/10/1
 */

@Data
public class CountCompleteType {
    /**
     *调查绑定的人员组组名
     */
    private String groupName;

    /**
     * 人员组中小组的组名,即人员类型,integer表示未完成的人数；
     */
    private Map<String,Integer> typeCount;
}
