package com.itobase.questionnaire.VO;

import lombok.Data;


/**
 * @author liucong
 * @date 2018/8/6
 */
@Data
public class ClientSurvey {

    private String surveyId;


    /**
     * 调查名称
     */

    private String title;

    /**
     * 调查面向的部门；
     */
    private String section = "未设置部门";

    /**
     * 调查描述
     */
    private String description;

    /**
     * 问卷的xml字符串
     */
    private String xmlStr;

}
