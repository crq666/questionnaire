package com.itobase.questionnaire.VO;

import lombok.Data;

import java.util.List;

/**
 * @author: lphu
 * @create: 2018-10-19 21:43
 * @description:
 */
@Data
public class DataVO<T>
{
    private int sEcho; // 请求次数
    private long iTotalRecords;  // 记录总条数
    private long iTotalDisplayRecords;  // 页面显示数量
    private List<T> aaData;  // 返回给前端的数据
}
