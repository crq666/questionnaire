package com.itobase.questionnaire.VO;

import lombok.Data;

/**
 * 用于存储用户的基本信息和用户的满意度
 * @author liucong
 * @date 2018/9/1
 */
@Data
public class UserInfo implements Comparable<UserInfo> {

    private BaseInfo baseInfo;

    /**
     * 满意度
     */
    private Float satisfaction;

    private String submitId;

    @Override
    public int compareTo(UserInfo o) {
        if(this.getSatisfaction()!=null&&o.getSatisfaction()!=null){
            if(this.getSatisfaction().compareTo(o.getSatisfaction())==0){
                return o.getBaseInfo().getId().compareTo(this.getBaseInfo().getId());
            }else{
                return o.getSatisfaction().compareTo(this.getSatisfaction());
            }
        } else if(this.getSatisfaction()!=null){
            return -1;
        }else if(o.getSatisfaction()!=null){
            return 1;
        }else return o.getBaseInfo().getId().compareTo(this.getBaseInfo().getId());
    }
}
