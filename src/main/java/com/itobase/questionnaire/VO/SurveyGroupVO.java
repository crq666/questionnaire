package com.itobase.questionnaire.VO;

import lombok.Data;

import java.util.Map;

/**
 * @author: lphu
 * @create: 2018-10-08 10:44
 * @description:
 */
@Data
public class SurveyGroupVO
{
    private String surveyGroupId;
    private String surveyGroupName;
    private Map<String,Integer> group;
}

