package com.itobase.questionnaire.VO;

import lombok.Data;

import java.util.List;

/**
 * @author: lphu
 * @create: 2018-10-31 21:34
 * @description:
 */
@Data
public class SendVO {
    private String surveyId;
    private List<String> phone;
}
