package com.itobase.questionnaire.VO;

import com.itobase.questionnaire.model.auth.User;
import com.itobase.questionnaire.util.Convert;
import lombok.Data;

/**
 * @author: lphu
 * @create: 2018-10-20 12:32
 * @description:
 */
@Data
public class UserBaseVO implements Convert<UserBaseVO,User>
{
    private String id;

    private String username;

    private String name;

    private String collegeName;

    private String longPhone;

    private String gender;

    private String level;

    /**
     * 专业
     */
    private String major;

    /**
     * 班级名称
     */
    private String className;

    /**
     * 禁用
     */
    private boolean flag;
}
