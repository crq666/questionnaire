package com.itobase.questionnaire.VO;

import com.itobase.questionnaire.template.Page;
import lombok.Data;



/**
 * 包含了所有用户满意度和调查总满意度
 * @author liucong
 * @date 2018/9/2
 */
@Data
public class SurveyRate {

    private PageDatas<UserInfo> userInfoList;

    private Float totalRate;


}
