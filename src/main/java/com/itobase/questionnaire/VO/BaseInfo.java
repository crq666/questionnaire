package com.itobase.questionnaire.VO;

import lombok.Data;

/**
 * @author liucong
 * @date 2018/9/4
 */
@Data
public class BaseInfo {
    private String id;

    private String name;

    private String phone;

    private String type;

}
