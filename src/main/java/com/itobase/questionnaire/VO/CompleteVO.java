package com.itobase.questionnaire.VO;

import lombok.Data;

/**
 * @author liucong
 * @date 2018/9/4
 */
@Data
public class CompleteVO implements Comparable<CompleteVO> {

    private BaseInfo baseInfo;

    private String submitTime;

    /**
     * 对应的提交的id
     */
    private String submitId;

    /**
     * true表示已完成，false表示未完成
     */
    private Boolean complete;

    @Override
    public int compareTo(CompleteVO o) {
        if(this.getComplete()!=null&&o.getComplete()!=null){
            if(this.getComplete()==o.getComplete()){
                return o.getBaseInfo().getId().compareTo(this.getBaseInfo().getId());
            }else if(this.getComplete()){
                return -1;
            }else return 1;
        }else if(this.getComplete()!=null&&this.getComplete()){
            return -1;
        }else if(o.getComplete()!=null&&o.getComplete()){
            return 1;
        }else return o.getBaseInfo().getId().compareTo(this.getBaseInfo().getId());
    }
}
