package com.itobase.questionnaire.VO;

/**
 * @author liucong
 * @date 2018/10/12
 */
import java.io.Serializable;
import java.util.List;

public class PageDatas<E> implements Serializable{

    /**
     *
     */
    private static final long serialVersionUID =7216920941926389211L;
    // 总记录数
    private long total;
    // 总页数
    private int pages;
    // 当前页数据
    private List<E> datas;

    public PageDatas(long total, int pages, List<E> datas) {
        super();
        this.total = total;
        this.pages = pages;
        this.datas = datas;
    }

    public PageDatas(long total, int pages) {
        super();
        this.total = total;
        this.pages = pages;
    }


    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public List<E> getDatas() {
        return datas;
    }

    public void setDatas(List<E> datas) {
        this.datas = datas;
    }

}
