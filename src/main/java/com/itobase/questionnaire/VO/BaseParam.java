package com.itobase.questionnaire.VO;

import lombok.Data;

/**
 * @author liucong
 * @date 2018/8/4
 */
@Data
public class BaseParam {


    /**
     * 题号
     */
    private Integer orderNum;

    /**
     * 对应的值
     */
    private String value;

    /**
     * 答案是：true，答案不是：false
     */
    private Boolean is;
}
