package com.itobase.questionnaire.VO;

import com.itobase.questionnaire.model.auth.User;
import com.itobase.questionnaire.template.Constant;
import com.itobase.questionnaire.util.Convert;
import lombok.Data;

/**
 * @author: lphu
 * @create: 2018-10-03 22:51
 * @description:
 */
@Data
public class UserVO implements Convert<UserVO,User>
{
    private String id;

    private String username;

    private String name;

    private String gender;

    private String collegeName;

    private String longPhone;

    private String userType;

    /**
     * 类别组
     */
    private String group;

    private Boolean flag;

    private String finished;

    private String informed = Constant.DEFAULT_SEND;


}
