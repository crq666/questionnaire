package com.itobase.questionnaire.repository;

import com.itobase.questionnaire.model.Questionnaire;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.domain.Page;

import java.util.List;


/**
 * @author cong
 * @date 2018/7/29
 */


public interface QuesnaireRepository extends MongoRepository<Questionnaire, String> {

    Questionnaire findById(String id);

    Page<Questionnaire> findAll(Pageable pageable);

    Questionnaire findByName(String name);

    List<Questionnaire> findAllById(String id);

}
