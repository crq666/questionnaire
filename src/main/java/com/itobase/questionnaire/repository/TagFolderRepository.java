package com.itobase.questionnaire.repository;

import com.itobase.questionnaire.model.TagFolder;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * @author: lphu
 * @create: 2018-10-18 10:54
 * @description:
 */
public interface TagFolderRepository extends MongoRepository<TagFolder,String>
{
    TagFolder findByTagFolderName(String tagFolderName);
}
