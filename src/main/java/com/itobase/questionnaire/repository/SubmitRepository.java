package com.itobase.questionnaire.repository;

import com.itobase.questionnaire.model.Submission;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * @author liucong
 * @date 2018/7/30
 */
public interface SubmitRepository extends MongoRepository<Submission, String> {

    Submission findByUserIdAndSurveyId(String userId, String surveyId);

    List<Submission> findBySurveyId(String surveyId);

    Page<Submission> findBySurveyId(String surveyId, Pageable pageable);

    Submission findByUuidAndSurveyId(String uuid,String surveyId);

    List<Submission> findByUserId(String userId);


}
