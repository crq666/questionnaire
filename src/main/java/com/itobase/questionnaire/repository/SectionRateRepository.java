package com.itobase.questionnaire.repository;

import com.itobase.questionnaire.model.SectionRate;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * @author liucong
 * @date 2018/9/27
 */
public interface SectionRateRepository extends MongoRepository<SectionRate,String> {
    SectionRate findBySectionName(String sectionName);
}
