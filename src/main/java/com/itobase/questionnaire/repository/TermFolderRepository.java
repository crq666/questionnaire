package com.itobase.questionnaire.repository;

import com.itobase.questionnaire.model.TermFolder;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * @author: lphu
 * @create: 2018-10-18 10:56
 * @description:
 */
public interface TermFolderRepository extends MongoRepository<TermFolder,String>
{
}
