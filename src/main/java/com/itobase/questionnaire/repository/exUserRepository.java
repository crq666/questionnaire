package com.itobase.questionnaire.repository;

import com.itobase.questionnaire.model.auth.User;

import java.util.List;

/**
 * @author liucong
 * @date 2018/10/12
 */
public interface exUserRepository {

    List<User> findUserByKeyword(String keyword);

    List<User> findUserByIdsAndKeyword(String keyword,List<String> ids);
}
