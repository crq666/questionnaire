package com.itobase.questionnaire.repository;

import com.itobase.questionnaire.model.Teacher;
import com.itobase.questionnaire.model.auth.User;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;


/**
 * @author liucong
 * @date 2018/7/28
 */
public interface UserRepository extends MongoRepository<User, String> {

    User findByUsername(String username);

    Page<User> findByUserTypeId(String userTypeId, Pageable pageable);

    List<User> findByIdIn(String[] ids);

    Page<User> findByIdIn(Pageable pageable,String[] ids);

    User findByName(String name);

    Page<User> findByCollegeName(String collegeName,Pageable pageable);

    List<User> findByUserTypeId(String userTypeId);

    @Query("{'username':{'$regex': ?0 } }")
    List<User> findForeignUser(String key);
    User findByLongPhone(String longPhone);

}
