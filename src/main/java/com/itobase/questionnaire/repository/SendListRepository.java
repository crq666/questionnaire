package com.itobase.questionnaire.repository;

import com.itobase.questionnaire.model.SendList;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * @author: lphu
 * @create: 2018-10-31 19:39
 * @description:
 */
public interface SendListRepository extends MongoRepository<SendList, String>
{
    void findBySurveyId(String surveyId);
}
