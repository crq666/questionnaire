package com.itobase.questionnaire.repository;

import com.itobase.questionnaire.model.Student;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * @author: lphu
 * @create: 2018-07-30 16:19
 * @description:
 */
public interface StudentRepository extends MongoRepository<Student, String> {

}
