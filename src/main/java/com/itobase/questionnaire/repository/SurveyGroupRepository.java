package com.itobase.questionnaire.repository;

import com.itobase.questionnaire.model.SurveyGroup;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * @author: lphu
 * @create: 2018-08-31 14:46
 * @description:
 */
public interface SurveyGroupRepository extends MongoRepository<SurveyGroup,String>
{
    SurveyGroup findByGroupName(String groupName);

}
