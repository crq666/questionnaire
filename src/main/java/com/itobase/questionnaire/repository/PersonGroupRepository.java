package com.itobase.questionnaire.repository;

import com.itobase.questionnaire.model.PersonGroup;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * @author: lphu
 * @create: 2018-08-31 14:18
 * @description:
 */

public interface PersonGroupRepository extends MongoRepository<PersonGroup,String>
{
    PersonGroup findByGroupName(String groupName);
}
