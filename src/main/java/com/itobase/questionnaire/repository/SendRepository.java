package com.itobase.questionnaire.repository;

import com.itobase.questionnaire.model.SendPool;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface SendRepository extends MongoRepository<SendPool,String> {

    Page<SendPool> findAll(Pageable pageable);

    List<SendPool> findByIdIn(String[] args);

    void deleteByIdIn(String[] args);

    void deleteByPhoneNumbrIn(String[] args);

    SendPool findByPhoneNumbrAndSurvey(String number,String survey);

    List<SendPool> findAllByStatus(String status);

    SendPool findByPhoneNumbr(String number);

    void deleteAllBySurvey(String survey);


}
