package com.itobase.questionnaire.repository.Impl;

import com.itobase.questionnaire.model.Student;
import com.itobase.questionnaire.model.auth.User;
import com.itobase.questionnaire.repository.exUserRepository;
import org.apache.poi.ss.formula.functions.T;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;

import static org.springframework.data.mongodb.core.query.Criteria.where;

/**
 * @author liucong
 * @date 2018/10/12
 */
@Repository
public class exUserRepositoryImpl implements exUserRepository {


    @Autowired
    MongoTemplate mongoTemplate;


    public List<User> findUserByKeyword(String keyword){
        Criteria keywordCriteria = new Criteria().orOperator(
                where("username").regex(keyword),
                where("name").regex(keyword),
                where("gender").regex(keyword),
                where("collegeName").regex(keyword),
                where("longPhone").regex(keyword),
                where("shortPhone").regex(keyword),
                where("tag").elemMatch(where("$eq").is(keyword))
        );
        Criteria studentCriteria = new Criteria().orOperator(
                where("grade").regex(keyword),
                where("major").regex(keyword),
                where("className").regex(keyword),
                keywordCriteria
        );
        List<User> userList = mongoTemplate.find(Query.query(keywordCriteria),User.class);
        userList.addAll(mongoTemplate.find(Query.query(studentCriteria), Student.class));
        LinkedHashSet<User> userLinkedHashSet = new LinkedHashSet<>(userList);
        userList.clear();
        userList.addAll(userLinkedHashSet);
        return userList;
    }

    public List<User> findUserByIdsAndKeyword(String keyword,List<String> ids){
        List<ObjectId> objectIdList = new ArrayList<>();
        ids.forEach(e->objectIdList.add(new ObjectId(e)));
        Criteria keywordCriteria = new Criteria().orOperator(
                where("username").regex(keyword),
                where("name").regex(keyword),
                where("gender").regex(keyword),
                where("collegeName").regex(keyword),
                where("longPhone").regex(keyword),
                where("shortPhone").regex(keyword),
                where("tag").elemMatch(where("$eq").is(keyword))
        );
        Criteria studentCriteria = new Criteria().orOperator(
                where("grade").regex(keyword),
                where("major").regex(keyword),
                where("className").regex(keyword),
                keywordCriteria
        ).and("_id").in(objectIdList);
        keywordCriteria.and("_id").in(objectIdList);
        Query query = Query.query(keywordCriteria);
        List<User> userList = mongoTemplate.find(query,User.class);
        userList.addAll(mongoTemplate.find(Query.query(studentCriteria), Student.class));
        LinkedHashSet<User> userLinkedHashSet = new LinkedHashSet<>(userList);
        userList.clear();
        userList.addAll(userLinkedHashSet);
        return userList;
    }


}
