package com.itobase.questionnaire.repository;

import com.itobase.questionnaire.model.ShortUrl;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * @author liucong
 * @date 2018/9/25
 */
public interface UrlRepository extends MongoRepository<ShortUrl,String> {

    ShortUrl findBySurveyId(String surveyId);

    ShortUrl findByShortUrl(String shortUrl);


}
