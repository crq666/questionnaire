package com.itobase.questionnaire.repository;

import com.itobase.questionnaire.model.Tag;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * @author: lphu
 * @create: 2018-08-01 14:26
 * @description:
 */
public interface TagRepository extends MongoRepository<Tag, String> {
    Tag findByTagName(String tagName);

    List<Tag> findByTagFolderId(String tagFolderId);
}
