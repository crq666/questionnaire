package com.itobase.questionnaire.repository;

import com.itobase.questionnaire.model.Teacher;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * @author: lphu
 * @create: 2018-07-30 16:29
 * @description:
 */
public interface TeacherRepository extends MongoRepository<Teacher, String> {

}
