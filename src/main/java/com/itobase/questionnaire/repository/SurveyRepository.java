package com.itobase.questionnaire.repository;

import com.itobase.questionnaire.model.EnumList.States;
import com.itobase.questionnaire.model.Survey;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * @author liucong
 * @date 2018/7/30
 */
public interface SurveyRepository extends MongoRepository<Survey, String> {

    Page<Survey> findAll(Pageable pageable);

    List<Survey> findByPrivately(Boolean privately, Sort sort);

    Survey findById(String id);

    Survey findByTitle(String name);

    List<Survey> findByPrivatelyAndState(Boolean privately,States state,Sort sort);

    //List<Survey> findByPrivatelyAndSatisfy(Boolean privately);

    List<Survey> findByPrivatelyOrState(Boolean privately, States state,Sort sort);

    //List<Survey> findByState(States state);

    List<Survey> findBySurveyGroupId(String surveyGroupId);

    Survey findBySection(String sectionName);


}
