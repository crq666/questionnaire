package com.itobase.questionnaire.service;

import com.itobase.questionnaire.dto.UpdateTagFolderDTO;
import com.itobase.questionnaire.model.TagFolder;

import java.util.List;

/**
 * @author: lphu
 * @create: 2018-10-18 11:00
 * @description:
 */
public interface ITagFolderService
{

    void addTagFolder(TagFolder tagFolder);

    void deleteTagFolder(String tagFolderId);

    void updateTagFolder(UpdateTagFolderDTO updateTagFolderDTO);

    List<TagFolder> getTagFolderList();
}
