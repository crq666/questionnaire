package com.itobase.questionnaire.service;


import com.itobase.questionnaire.VO.BaseInfo;
import com.itobase.questionnaire.VO.UserBaseVO;
import com.itobase.questionnaire.VO.UserVO;
import com.itobase.questionnaire.dto.MarkDTO;
import com.itobase.questionnaire.model.auth.User;
import com.itobase.questionnaire.template.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

public interface IUserService {

    User findByUsername(String username);

    Integer insert(User user);

    List<User> getUsersByCriteria(String conditions);

    List<BaseInfo> getUserInfoByIds(List<String> ids,String surveyGroupId);

    Integer addUsersByExcel(MultipartFile file,String userTypeId);

    Page<UserBaseVO> getUser(Pageable pageable, String userTypeId, String keyWord);

    void offUser(String userId);

    void deleteUser(String userId);

    void mark(MarkDTO markDTO);

    void check();

    Map<String,List<User>> getRangeUser(Integer number,String userTypeId);

    Map<String,List<String>> getRangeId(Integer number,String userTypeId);


}
