package com.itobase.questionnaire.service;

import com.alibaba.fastjson.JSONObject;
import com.itobase.questionnaire.VO.ClientSurvey;


/**
 * @author liucong
 * @date 2018/11/6
 */
public interface IClientService {

    JSONObject submitList() throws Throwable;

    JSONObject surveyList() throws Throwable;

    ClientSurvey getClientSurvey(String surveyId) throws Throwable;
}
