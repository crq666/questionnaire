package com.itobase.questionnaire.service;

import com.alibaba.fastjson.JSONObject;
import com.itobase.questionnaire.model.auth.User;

import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.MalformedParameterizedTypeException;
import java.util.List;
import java.util.Map;

/**
 * @author cong
 * @date 2018/7/31
 */
public interface IResultService {
    /**
     * 预览一个人的问卷填写结果
     *
     * @return
     */
    JSONObject getResultPerson(String submitId);

    Integer getRateExcel(String surveyId, HttpServletResponse response);


    Integer getResultExcel(HttpServletResponse response);

    void getUnsubTeacher(HttpServletResponse response,String surveyGroupId);

    Map<String,Map<String,Object>> getResult();
}
