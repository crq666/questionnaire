package com.itobase.questionnaire.service;

import com.itobase.questionnaire.model.PersonGroup;

import java.util.List;

/**
 * @author: lphu
 * @create: 2018-08-31 14:19
 * @description:
 */

public interface IPersonGroupService
{
     Integer addGroup(PersonGroup personGroup);

     List<PersonGroup> getGroup();
}
