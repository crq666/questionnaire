package com.itobase.questionnaire.service;

import java.util.List;

/**
 * @author liucong
 * @date 2018/9/8
 */
public interface ISendService {

    Integer initSent(String loginName,String password,String schoolCode);


    Integer send(String loginName,String password,String schoolCode,String msg, List<String> phones);





}
