package com.itobase.questionnaire.service;

import com.alibaba.fastjson.JSONObject;
import com.itobase.questionnaire.VO.*;
import com.itobase.questionnaire.model.Submission;
import com.itobase.questionnaire.model.Survey;
import com.itobase.questionnaire.model.auth.User;
import com.itobase.questionnaire.util.Message;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author cong
 * @date 2018/7/29
 */
public interface ISubmitService {

    Message addSubmission(Submission submission, HttpServletRequest request) throws Throwable;

    PageDatas<CompleteVO> getUserByComplete(String id, List<User> users, String criteria, Pageable pageable, String keyword);

    Integer getResultExcel(String id, HttpServletResponse response);

    JSONObject getSubmitTime(String surveyId, List<BaseParam> params);

    JSONObject getStatics(String surveyId, Integer orderNum, Page<Submission> pageSubmits);

    SurveyRate getSatisfaction(String surveyId, String criteria,Pageable pageable,String keyword);

    PageDatas<CompleteVO> getCompleteAnonymous(String surveyId,Pageable pageable);

    CountCompleteType getTypeCompleteCount(Survey survey);

    Integer getSubmitsCount(String surveyId);

}
