package com.itobase.questionnaire.service;

import com.itobase.questionnaire.model.Questionnaire;

/**
 * @author liucong
 * @date 2018/8/4
 */
public interface IQuesnaireService {
    Questionnaire findById(String id);

    Questionnaire update(Questionnaire questionnaire);

    void delete(String id);

    Questionnaire insert(Questionnaire questionnaire);

    Questionnaire save(Questionnaire questionnaire);
}
