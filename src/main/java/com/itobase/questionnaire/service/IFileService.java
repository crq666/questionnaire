package com.itobase.questionnaire.service;

import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author cong
 * @date 2018/7/29
 */
public interface IFileService {

    String fileUpload(MultipartFile file) throws IOException;

    void download(String id, HttpServletResponse response, HttpServletRequest request) throws IOException;

}
