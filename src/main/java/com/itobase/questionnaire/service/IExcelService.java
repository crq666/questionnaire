package com.itobase.questionnaire.service;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

/**
 * @author htyu
 * @date 2018/7/30
 */
public interface IExcelService {


    List<String> upLoad(MultipartFile file) throws IOException;
}
