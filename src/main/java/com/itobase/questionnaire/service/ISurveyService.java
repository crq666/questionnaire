package com.itobase.questionnaire.service;

import com.itobase.questionnaire.VO.ClientSurvey;
import com.itobase.questionnaire.model.EnumList.States;
import com.itobase.questionnaire.model.Survey;
import com.itobase.questionnaire.util.Message;
import org.springframework.web.multipart.MultipartFile;
import redis.clients.jedis.Client;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

/**
 * @author liucong
 * @date 2018/7/30
 */
public interface ISurveyService {


//    Message build(Survey survey);


    String updateQuestionnaire(String id,MultipartFile file) throws IOException;

    Message updateTime(String id,String startTime,String endTime) throws ParseException;

    ClientSurvey getClientSurvey(Survey survey);

    void deleteSendPool(String survey);


}
