package com.itobase.questionnaire.service;

import com.itobase.questionnaire.VO.PageDatas;
import com.itobase.questionnaire.VO.SurveyGroupVO;
import com.itobase.questionnaire.VO.UserVO;
import com.itobase.questionnaire.dto.GroupUpdateDTO;
import com.itobase.questionnaire.dto.SurveyGroupUpdateDTO;
import com.itobase.questionnaire.dto.UserDTO;
import com.itobase.questionnaire.model.SurveyGroup;
import com.itobase.questionnaire.template.Page;
import javassist.compiler.ast.Keyword;
import org.springframework.data.domain.Pageable;


import java.util.List;
import java.util.Map;

/**
 * @author: lphu
 * @create: 2018-09-02 15:06
 * @description:
 */


public interface ISurveyGroupService
{
    List<SurveyGroup> getSurveyGroupList();

    void addGroup(SurveyGroup surveyGroup);

    List<UserVO> getUsersBySurveyGroupId(String surveyGroupId, String group,String keyWord);

    PageDatas<UserVO> getUsersBySurveyId(Pageable pageable, String surveyId, String group, String keyWord);

    void addUsers(UserDTO userDTO);

    /**
     * 根据调查组id获得userId
     * @param surveyGroupId
     * @return
     */
    List<String> getUsersById(String surveyGroupId);

    void deleteBySurveyGroupId(String surveyGroupId);

    Map<String,List<String>> getGroupById(String surveyGroupId);

    void deleteUsers(UserDTO userDTO);

    /**
     * 新建类别组
     * @param surveyGroupId
     * @param group
     */
    void addGroup(String surveyGroupId,String group);

    void deleteGroup(String surveyGroupId,String group);

    SurveyGroupVO getUsersBySurveyGroupId(String surveyGroupId);

    void addOtherUsers(UserDTO userDTO);

    void updateSurveyGroup(SurveyGroupUpdateDTO surveyGroupUpdateDTO);

    void updateGroup(GroupUpdateDTO groupUpdateDTO);

}
