package com.itobase.questionnaire.service;

import com.itobase.questionnaire.dto.UpdateTagDTO;
import com.itobase.questionnaire.model.Tag;

import java.util.List;

/**
 * @author: lphu
 * @create: 2018-08-01 14:23
 * @description:
 */
public interface ITagService {

    void addTag(Tag tag);

    void deleteTag(String tagId);

    void updateTag(UpdateTagDTO updateTagDTO);

    List<Tag> getTagList(String tagFolderId);
}
