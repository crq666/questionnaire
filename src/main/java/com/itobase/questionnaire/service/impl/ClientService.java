package com.itobase.questionnaire.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.itobase.questionnaire.VO.ClientSurvey;
import com.itobase.questionnaire.model.Submission;
import com.itobase.questionnaire.model.Survey;
import com.itobase.questionnaire.repository.SubmitRepository;
import com.itobase.questionnaire.repository.SurveyRepository;
import com.itobase.questionnaire.service.IClientService;
import com.itobase.questionnaire.util.AuthUtil;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.match;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.project;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.sort;
import static org.springframework.data.mongodb.core.query.Criteria.where;


/**
 * @author liucong
 * @date 2018/11/6
 */
@Service
public class ClientService implements IClientService {

    @Autowired
    SubmitRepository submitRepository;

    @Autowired
    SurveyRepository surveyRepository;

    @Autowired
    MongoTemplate mongoTemplate;

    @Autowired
    SurveyService surveyService;



    @Override
    public JSONObject submitList() throws Throwable {
        String userId = AuthUtil.getUserId();

        Criteria criteria = Criteria.where("userId").is(userId);
        Query query = Query.query(criteria).with(new Sort(Sort.Direction.DESC,"submitTime"));
        int total = 0;
        List<Submission> submissionList =  mongoTemplate.find(query,Submission.class);
        JSONObject submitAndId = new JSONObject();
        Map<String,String> submitDetails = new HashMap<>();
        for (Submission submission : submissionList) {
            Survey survey = surveyRepository.findOne(submission.getSurveyId());
            if (survey != null) {
                total++;
                submitDetails.put(survey.getTitle(),submission.getId());
            }
        }
        submitAndId.put("submits",submitDetails);
        submitAndId.put("total",total);
        return submitAndId;
    }

    @Override
    public JSONObject surveyList() throws Throwable {
        String userId = AuthUtil.getUserId();
        Criteria criteria = Criteria.where("userList").elemMatch(where("_id").is(new ObjectId(userId))).and("state").is("running");
        Aggregation aggregation = Aggregation.newAggregation(
                match(criteria),
                project("title","id","createTime","state"),
                sort(Sort.Direction.DESC,"createTime")
        );
        int total = 0;
        List<Survey> surveyList = mongoTemplate.aggregate(aggregation,"survey",Survey.class).getMappedResults();
        List<Survey> needComplete = new ArrayList<>();
        for (Survey survey : surveyList) {
            if (submitRepository.findByUserIdAndSurveyId(userId, survey.getId()) == null) {
                needComplete.add(survey);
                total++;
            }
        }
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("survey",needComplete);
        jsonObject.put("total",total);
        return jsonObject;
    }

    @Override
    public ClientSurvey getClientSurvey(String surveyId) {
        Survey survey = surveyRepository.findOne(surveyId);
        return surveyService.getClientSurvey(survey);
    }
}
