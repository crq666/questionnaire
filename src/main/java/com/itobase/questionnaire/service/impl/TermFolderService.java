package com.itobase.questionnaire.service.impl;

import com.itobase.questionnaire.repository.TermFolderRepository;
import com.itobase.questionnaire.service.ITermFolderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author: lphu
 * @create: 2018-10-18 11:01
 * @description:
 */

@Service
public class TermFolderService implements ITermFolderService
{
    @Autowired
    TermFolderRepository termFolderRepository;
}
