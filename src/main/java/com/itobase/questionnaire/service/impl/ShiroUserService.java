package com.itobase.questionnaire.service.impl;

import com.itobase.questionnaire.VO.BaseInfo;
import com.itobase.questionnaire.VO.UserBaseVO;
import com.itobase.questionnaire.dto.MarkDTO;
import com.itobase.questionnaire.model.auth.User;
import com.itobase.questionnaire.repository.UserRepository;
import com.itobase.questionnaire.service.IUserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

import java.util.List;

/**
 * @author liucong
 * @date 2018/7/24
 */
@Service
public class ShiroUserService implements IUserService {

    @Autowired
    UserRepository userRepository;


    @Override
    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public Integer insert(User user) {
        return null;
    }

    @Override
    public List<User> getUsersByCriteria(String conditions) {
        return null;

    }

    @Override
    public List<BaseInfo> getUserInfoByIds(List<String> ids, String surveyGroup) {
        return null;
    }

    @Override
    public Integer addUsersByExcel(MultipartFile file, String userTypeId) {
        return null;
    }


    public Integer addTag(String userId, String tagId) {
        return null;
    }

    @Override
    public com.itobase.questionnaire.template.Page<UserBaseVO> getUser(Pageable pageable, String userTypeId, String keyWord) {
        return null;
    }


    @Override
    public void offUser(String id) {

    }

    @Override
    public void deleteUser(String userId) {

    }

    @Override
    public void mark(MarkDTO markDTO) {

    }

    @Override
    public void check() {

    }

    @Override
    public Map<String, List<User>> getRangeUser(Integer number, String userTypeId) {
        return null;
    }

    @Override
    public Map<String, List<String>> getRangeId(Integer number, String userTypeId) {
        return null;
    }


}
