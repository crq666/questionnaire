package com.itobase.questionnaire.service.impl;

import com.itobase.questionnaire.model.PersonGroup;
import com.itobase.questionnaire.repository.PersonGroupRepository;
import com.itobase.questionnaire.service.IPersonGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author: lphu
 * @create: 2018-08-31 14:20
 * @description:
 */

@Service
public class PersonGroupService implements IPersonGroupService
{
    @Autowired
    PersonGroupRepository personGroupRepository;

    @Override
    public Integer addGroup(PersonGroup personGroup)
    {

        personGroupRepository.insert(personGroup);
        return 1;
    }

    /**
     * 获取人员组列表
     * @return
     */
    public List<PersonGroup> getGroup()
    {
        return personGroupRepository.findAll();
    }
}
