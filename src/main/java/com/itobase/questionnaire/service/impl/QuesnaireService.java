package com.itobase.questionnaire.service.impl;

import com.itobase.questionnaire.model.Questionnaire;
import com.itobase.questionnaire.repository.QuesnaireRepository;
import com.itobase.questionnaire.service.IQuesnaireService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

/**
 * 主要目的是将xmlStr保存在缓存中,因此每个操作questionnaire的操作必须通过这个service。
 *
 * @author liucong
 * @date 2018/8/4
 */
@Service
public class QuesnaireService implements IQuesnaireService {


    @Autowired
    QuesnaireRepository quesnaireRepository;


    /**
     * 根据id查找问卷
     *
     * @param id 问卷id
     * @return questionnaire
     */
    @Cacheable(cacheNames = "xmlStr", key = "#id")
    @Override
    public Questionnaire findById(String id) {
        return quesnaireRepository.findOne(id);
    }


    @CachePut(cacheNames = "xmlStr", key = "#questionnaire.id")
    @Override
    public Questionnaire update(Questionnaire questionnaire) {
        return quesnaireRepository.save(questionnaire);
    }

    @CacheEvict(cacheNames = "xmlStr", key = "#id")
    @Override
    public void delete(String id) {
        quesnaireRepository.delete(id);
    }

    @CachePut(cacheNames = "xmlStr", key = "#questionnaire.id")
    @Override
    public Questionnaire insert(Questionnaire questionnaire) {
        return quesnaireRepository.insert(questionnaire);
    }

    @CachePut(cacheNames = "xmlStr", key = "#questionnaire.id")
    @Override
    public Questionnaire save(Questionnaire questionnaire) {
        return quesnaireRepository.save(questionnaire);
    }


}
