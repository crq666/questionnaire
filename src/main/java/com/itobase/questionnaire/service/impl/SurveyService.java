package com.itobase.questionnaire.service.impl;

import com.itobase.questionnaire.VO.ClientSurvey;
import com.itobase.questionnaire.model.EnumList.States;
import com.itobase.questionnaire.model.Questionnaire;
import com.itobase.questionnaire.model.Survey;
import com.itobase.questionnaire.repository.SendRepository;
import com.itobase.questionnaire.repository.SurveyRepository;
import com.itobase.questionnaire.service.IExcelService;
import com.itobase.questionnaire.service.ISurveyService;
import com.itobase.questionnaire.util.DateUtil;
import com.itobase.questionnaire.util.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.text.ParseException;
import java.util.Date;
import java.util.List;


/**
 * @author liucong
 * @date 2018/7/30
 */
@Service
public class SurveyService implements ISurveyService {
    @Autowired
    SurveyRepository surveyRepository;

    @Autowired
    IExcelService iExcelService;

    @Autowired
    QuesnaireService quesnaireService;

    @Autowired
    SendRepository sendRepository;





    @Override

    public String  updateQuestionnaire(String id,MultipartFile file) throws IOException {
        List<String> list = iExcelService.upLoad(file);
        String id_new = list.get(0);
        Survey survey = surveyRepository.findById(id);
        if(survey==null){
            return "调查id出错，未找到该调查";
        }
        String id_old = survey.getQuestionsID();
        Questionnaire questionnaire = quesnaireService.findById(id_old);
        if(questionnaire!=null) {
            questionnaire.setExpired(true);
            quesnaireService.update(questionnaire);
        }
        survey.setQuestionsID(id_new);
        surveyRepository.save(survey);
        return "更新成功";

    }

    @Override
    public Message updateTime(String id,String startTime,String endTime) throws ParseException {
        Survey survey = surveyRepository.findById(id);
        if(survey==null){
            return Message.createSuc("调查id有误，未找到相关调查");
        }else{
        if(survey.getStartTime().length()==0||survey.getEndTime().length()==0){
            return Message.createErr("调查周期不能为空");
        }
        Date current_day = new Date();
        survey.setStartTime(startTime);
        survey.setEndTime(endTime);
        Date begin_day = DateUtil.parse_day(startTime);
        Date end_day = DateUtil.parse_day(endTime);
        if(begin_day.compareTo(end_day)>=0){
            return Message.createErr("调查开始时间不能大于结束时间");
        }
        if (current_day.compareTo(begin_day) > 0 && current_day.compareTo(end_day) < 0) {
            survey.setState(States.running);
        } else if (current_day.compareTo(begin_day) < 0) {
            survey.setState(States.notStarted);
        } else if (current_day.compareTo(end_day) > 0) {
            survey.setState(States.ended);
        }
        return Message.createSuc(surveyRepository.save(survey));
    }
    }

    @Override
    public ClientSurvey getClientSurvey(Survey survey) {
        ClientSurvey clientSurvey = new ClientSurvey();
        clientSurvey.setSurveyId(survey.getId());
        clientSurvey.setDescription(survey.getDescription());
        clientSurvey.setTitle(survey.getTitle());
        if(survey.getSection()!=null){
            clientSurvey.setSection(survey.getSection());
        }
        Questionnaire questionnaire = quesnaireService.findById(survey.getQuestionsID());
        if(questionnaire.getXmlStr()==null||questionnaire.getXmlStr().equals("")){
            questionnaire.setXmlStr("上传问卷时未设置问卷的xmlStr");
        }
        clientSurvey.setXmlStr(questionnaire.getXmlStr());
        return clientSurvey;
    }

    //当删除调查时，清空通知池内相关人员
    @Override
    public void deleteSendPool(String title){

        sendRepository.deleteAllBySurvey(title);


    }




}
