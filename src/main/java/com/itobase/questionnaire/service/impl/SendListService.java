package com.itobase.questionnaire.service.impl;

import com.itobase.questionnaire.VO.SendVO;
import com.itobase.questionnaire.model.SendList;
import com.itobase.questionnaire.model.Survey;
import com.itobase.questionnaire.model.SurveyGroup;
import com.itobase.questionnaire.model.auth.User;
import com.itobase.questionnaire.repository.SendListRepository;
import com.itobase.questionnaire.repository.SurveyGroupRepository;
import com.itobase.questionnaire.repository.SurveyRepository;
import com.itobase.questionnaire.repository.UserRepository;
import com.itobase.questionnaire.service.ISendListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * @author: lphu
 * @create: 2018-10-31 19:43
 * @description:
 */

@Service
public class SendListService implements ISendListService
{
    @Autowired
    UserRepository userRepository;
    @Autowired
    SurveyGroupRepository surveyGroupRepository;
    @Autowired
    SurveyRepository surveyRepository;
    @Autowired
    SendListRepository sendListRepository;
    @Autowired
    UrlService urlService;

    @Override
    public void insertNeedSend()
    {
        List<Survey> surveyList = surveyRepository.findAll();
        List<SendList> sendLists = new ArrayList<>();
        for (Survey survey : surveyList)
        {
            if (survey.getSurveyGroupId()==null)
                continue;
            SurveyGroup SurveyGroup = surveyGroupRepository.findOne(survey.getSurveyGroupId());
            for (Map.Entry<String,List<String>> entry : SurveyGroup.getGroup().entrySet())
            {
                for (String userId : entry.getValue())
                {
                    User user = userRepository.findOne(userId);
                    SendList sendList = new SendList();
                    sendList.setLongPhone(user.getLongPhone());
                    sendList.setUsername(user.getUsername());
                    sendList.setName(user.getName());
                    sendList.setUserId(user.getId());
                    sendList.setSurveyId(survey.getId());
                    sendList.setUserTypeId(user.getUserTypeId());
                    sendLists.add(sendList);
                }
            }
        }
        sendListRepository.save(removeTeacherDuplicate(sendLists));
    }


    @Override
    public Boolean send() throws InterruptedException {
        List<SendList> sendLists = sendListRepository.findAll();
        Set set = new HashSet();
        Map<String,List<String>> map = new HashMap<>();
        List<SendVO> sendVOS = new ArrayList<>();
        sendLists.stream().collect(Collectors.groupingBy(SendList::getSurveyId)).entrySet().forEach(entry->{
            List<SendList> sendVOList = entry.getValue();
            List<String> phoneList = new ArrayList<>();
            sendVOList.forEach(e->phoneList.add(e.getLongPhone()));
            map.put(entry.getKey(),phoneList);
        });

        ExecutorService executorService = Executors.newSingleThreadExecutor();
        executorService.submit(() -> {
            try {
                for (Map.Entry<String,List<String>> entry : map.entrySet()){
                    urlService.sendTest(entry.getValue(),entry.getKey());
                    System.out.println("发送一批"+entry.getKey()+">>>>>>>>>");
                    TimeUnit.SECONDS.sleep(60);
                }
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }
        });
       return true;
    }

    public List removeTeacherDuplicate(List<SendList> lists)
    {
        Set set = new HashSet();
        List<SendList> newSendLists = new ArrayList<>();
        Collections.shuffle(lists);
        for (SendList sendList : lists)
        {
          if (sendList.getUserTypeId().equals("5b88e7e8dc8b5a1a84a96368"))
          {
              //根据学号
              if (set.add(sendList.getUsername()))
                  newSendLists.add(sendList);
          }else {
              newSendLists.add(sendList);
          }
        }
        return newSendLists;
    }




}
