package com.itobase.questionnaire.service;

import java.util.List;
import java.util.Map;

/**
 * @author: lphu
 * @create: 2018-10-31 19:43
 * @description:
 */

public interface ISendListService
{
    void insertNeedSend();

    Boolean send() throws InterruptedException;
}
