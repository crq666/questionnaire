package com.itobase.questionnaire.service;

import com.itobase.questionnaire.model.Submission;
import com.itobase.questionnaire.model.Survey;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import javax.servlet.http.HttpServletResponse;

/**
 * @author liucong
 * @date 2018/10/27
 */
public interface IOutcomeService {

    Float calculateRate(Submission submission);

    HSSFCellStyle getColumnTopStyle(HSSFWorkbook workbook);

    HSSFCellStyle getStyle(HSSFWorkbook workbook);

    void writeToResponse(HttpServletResponse response, HSSFWorkbook workbook,String excelName);

    String getSurveyContentRate(Survey survey);

}
