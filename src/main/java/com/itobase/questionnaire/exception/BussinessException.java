package com.itobase.questionnaire.exception;

/**
 * @author: lphu
 * @create: 2018-11-10 15:27
 * @description:
 */

public class BussinessException extends RuntimeException
{
    public BussinessException(){super();}

    public BussinessException(String message)
    {
        super(message);
    }
}
