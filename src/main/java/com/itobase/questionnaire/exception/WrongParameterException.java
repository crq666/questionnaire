package com.itobase.questionnaire.exception;

/**
 * @author: lphu
 * @create: 2018-09-03 14:41
 * @description:
 */

public class WrongParameterException extends RuntimeException
{
    public WrongParameterException(){super();}

    public WrongParameterException(String message)
    {
        super(message);
    }
}
