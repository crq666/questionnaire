package com.itobase.questionnaire.controller;

import com.itobase.questionnaire.dto.GroupUpdateDTO;
import com.itobase.questionnaire.dto.SurveyGroupDTO;
import com.itobase.questionnaire.dto.SurveyGroupUpdateDTO;
import com.itobase.questionnaire.dto.UserDTO;
import com.itobase.questionnaire.model.SurveyGroup;
import com.itobase.questionnaire.service.ISurveyGroupService;
import com.itobase.questionnaire.util.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;


/**
 * @author: lphu
 * @create: 2018-09-02 15:08
 * @description:
 */
@RestController
@RequestMapping("surveyGroup")
public class SurveyGroupController
{
    @Autowired
    ISurveyGroupService iSurveyGroupService;

    /**
     * 根据调查组id获取用户
     * @param surveyGroupId
     * @return
     */
    @GetMapping("getUsers")
    public Message getUsersBySurveyGroupId(@RequestParam(required = true)  String surveyGroupId,
                                           @RequestParam(required = false) String group,
                                           @RequestParam(required = false) String keyWord)
    {
        return Message.createSuc(iSurveyGroupService.getUsersBySurveyGroupId(surveyGroupId,group,keyWord));
    }

    /**
     * 根据调查id获取用户 有短信发送情况
     * @param surveyId
     * @param group
     * @return
     */
    @GetMapping("getUsers2")
    public Message getUsersBySurveyId(@PageableDefault Pageable pageable,
                                      @RequestParam String surveyId,
                                      @RequestParam(required = false) String group,
                                      @RequestParam(required = false) String keyWord)
    {
        return Message.createSuc(iSurveyGroupService.getUsersBySurveyId(pageable,surveyId,group,keyWord));
    }
    /**
     * 新建调查小组
     * @param surveyGroup
     * @return
     */
    @PostMapping("add")
    public Message addGroup(@RequestBody SurveyGroup surveyGroup){
        iSurveyGroupService.addGroup(surveyGroup);
        return Message.createSuc("新建调查组完成");
    }

    /**
     * 新建类别组
     * @param surveyGroupDTO
     * @return
     */
    @PutMapping("addGroup")
    public Message addGroup(@RequestBody SurveyGroupDTO surveyGroupDTO){
        iSurveyGroupService.addGroup(surveyGroupDTO.getSurveyGroupId(),surveyGroupDTO.getGroup());
        return Message.createSuc("新建评委组成成功");
    }

    /**
     * 调查组中增加用户
     * @param userDTO
     * @return
     */
    @PostMapping("addUser")
    public Message addUsers(@RequestBody UserDTO userDTO){
        //使用dto接受参数 直接map无法接受到参数
        iSurveyGroupService.addUsers(userDTO);
        return Message.createSuc("增加用户成功");
    }

    /**
     * 获取调查组列表
     * @return
     */
    @GetMapping("")
    public Message getSurveyGroupList()
    {
        return Message.createSuc(iSurveyGroupService.getSurveyGroupList());
    }

    /**
     * 获取调查组类别
     * @param surveyGroupId
     * @return
     */
    @GetMapping("getGroup")
    public Message getGroupById(@RequestParam String surveyGroupId){
        return Message.createSuc(iSurveyGroupService.getGroupById(surveyGroupId));
    }

    /**
     * 删除调查组
     * @param surveyGroupId
     * @return
     */
    @DeleteMapping("{surveyGroupId}")
    public Message deleteBySurveyGroupId(@PathVariable String surveyGroupId)
    {
        iSurveyGroupService.deleteBySurveyGroupId(surveyGroupId);
        return Message.createSuc("删除调查组成功!");
    }

    /**
     * 删除角色
     * @param userDTO
     * @return
     */
    @DeleteMapping("deleteUser")
    public Message deleteUsers(@RequestBody UserDTO userDTO)
    {
        iSurveyGroupService.deleteUsers(userDTO);
        return Message.createSuc("删除用户成功");
    }

    /**
     * 删除评委构成
     * @param surveyGroupDTO
     * @return
     */
    @DeleteMapping("deleteGroup")
    public Message deleteGroup(@RequestBody SurveyGroupDTO surveyGroupDTO)
    {
        iSurveyGroupService.deleteGroup(surveyGroupDTO.getSurveyGroupId(),surveyGroupDTO.getGroup());
        return Message.createSuc("删除评委构成成功");
    }

    /**
     * 根据调查组id获取用户信息
     * @param surveyGroupId
     * @return
     */
    @GetMapping("getGroup2")
    public Message getGroup(@RequestParam String surveyGroupId)
    {
        return Message.createSuc(iSurveyGroupService.getUsersBySurveyGroupId(surveyGroupId));
    }

    /**
     * 添加人到补充组
     * @param userDTO
     * @return
     */
    @PostMapping("addOtherUser")
    public Message addOtherUsers(@RequestBody UserDTO userDTO)
    {
        iSurveyGroupService.addOtherUsers(userDTO);
        return Message.createSuc("添加到补充组成功");
    }

    /**
     * 更新调查组名字
     * @param surveyGroupUpdateDTO
     * @return
     */
    @PutMapping("updateSurveyGroup")
    public Message updateSurveyGroup(@RequestBody SurveyGroupUpdateDTO surveyGroupUpdateDTO)
    {
        iSurveyGroupService.updateSurveyGroup(surveyGroupUpdateDTO);
        return Message.createSuc("修改调查组名字成功");
    }

    /**
     * 更新类别组名字
     * @param groupUpdateDTO
     * @return
     */
    @PutMapping("updateGroup")
    public Message updateGroup(@RequestBody GroupUpdateDTO groupUpdateDTO)
    {
        iSurveyGroupService.updateGroup(groupUpdateDTO);
        return Message.createSuc("修改类别组名字成功");
    }

    @GetMapping("template")
    public Message getTemplate(HttpServletResponse response){

        //1.设置文件ContentType类型，这样设置，会自动判断下载文件类型
        response.setContentType("multipart/form-data");
        //2.设置文件头：最后一个参数是设置下载文件名(假如我们叫a.pdf)
        response.setHeader("Content-Disposition", "attachment;fileName="+ "template.xls");
        ServletOutputStream out;
        //通过文件路径获得File对象(假如此路径中有一个download.pdf文件)
        File file = new File("template.xls");

        try {
            FileInputStream inputStream = new FileInputStream(file);

            //3.通过response获取ServletOutputStream对象(out)
            out = response.getOutputStream();

            int b = 0;
            byte[] buffer = new byte[512];
            while (b != -1){
                b = inputStream.read(buffer);
                //4.写到输出流(out)中
                out.write(buffer,0,b);
            }
            inputStream.close();
            out.close();
            out.flush();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return Message.createSuc("下载成功");
    }


}
