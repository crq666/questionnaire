package com.itobase.questionnaire.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @Description TODO
 * @Author 陈荣锵
 * @Date 11/7/2019 12:17 PM
 * @Version 1.0
 **/
public class LogController {
    Logger logger= LoggerFactory.getLogger(getClass());
    @GetMapping("/log")
    public void logTest(){
        logger.info("test:{}",123);
    }
}
