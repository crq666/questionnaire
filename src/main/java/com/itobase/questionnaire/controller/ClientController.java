package com.itobase.questionnaire.controller;


import com.itobase.questionnaire.service.IClientService;
import com.itobase.questionnaire.service.IResultService;
import com.itobase.questionnaire.util.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * @author liucong
 * @date 2018/11/6
 */
@RestController
@RequestMapping("client")
public class ClientController {

    @Autowired
    IClientService clientService;

    @Autowired
    IResultService resultService;

    /**
     * 根据登录人id获取已填写的所有调查
     * @return
     * @throws Throwable
     */
    @GetMapping("submitsName")
    public Message getSubmitByUser() throws Throwable {
        return Message.createSuc(clientService.submitList());
    }


    /**
     * 根据登录人id获取需要填写的所有调查
     * @return
     * @throws Throwable
     */
    @GetMapping("surveyName")
    public Message getNeedCompleteSurvey() throws Throwable {
        return Message.createSuc(clientService.surveyList());
    }

    /**
     * 手机访问调查controller
     * @param surveyId
     * @return
     * @throws Throwable
     */
    @GetMapping
    public Message getClientSurvey(@RequestParam String surveyId) throws Throwable {
        return Message.createSuc(clientService.getClientSurvey(surveyId));
    }

    @GetMapping("person")
    public Message getResultPerson(@RequestParam String submitId) {

        return Message.createSuc(resultService.getResultPerson(submitId));
    }




}
