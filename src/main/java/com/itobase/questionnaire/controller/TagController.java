package com.itobase.questionnaire.controller;

import com.itobase.questionnaire.dto.UpdateTagDTO;
import com.itobase.questionnaire.dto.UpdateTagFolderDTO;
import com.itobase.questionnaire.model.Tag;
import com.itobase.questionnaire.model.TagFolder;
import com.itobase.questionnaire.service.ITagFolderService;
import com.itobase.questionnaire.service.ITagService;
import com.itobase.questionnaire.util.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author: lphu
 * @create: 2018-08-01 14:22
 * @description:
 */

@RestController
@RequestMapping("tag")
public class TagController {

    @Autowired
    ITagService tagService;

    @Autowired
    ITagFolderService tagFolderService;


    /**
     * 添加标签夹
     * @param tagFolder
     * @return
     */
    @PostMapping("addFolder")
    public Message addTagFolder(@RequestBody TagFolder tagFolder)
    {
        tagFolderService.addTagFolder(tagFolder);
        return Message.createSuc("新建标签夹成功");
    }

    /**
     * 新建标签
     * @param tag
     * @return
     */
    @PostMapping("addTag")
    public Message addTag(@RequestBody Tag tag) {
        tagService.addTag(tag);
        return Message.createSuc("增加标签成功");
    }

    /**
     * 删除标签
     * @param tagId
     * @return
     */
    @DeleteMapping("{tagId}")
    public Message deleteTag(@PathVariable String tagId)
    {
        tagService.deleteTag(tagId);
        return Message.createSuc("删除标签成功");
    }

    /**
     * 批量删除标签
     * @param tagIds
     * @return
     */
    @DeleteMapping("delete")
    public Message deleteTagByIds(@RequestParam List<String> tagIds)
    {
        tagIds.forEach(e->tagService.deleteTag(e));
        return Message.createSuc("删除标签成功");
    }

    /**
     * 更新标签
     * @param updateTagDTO
     * @return
     */
    @PutMapping("update")
    public Message updateTag(@RequestBody UpdateTagDTO updateTagDTO)
    {
        tagService.updateTag(updateTagDTO);
        return Message.createSuc("更新成功");
    }

    /**
     * 更新标签夹
     * @param tagFolderDTO
     * @return
     */
    @PutMapping("updateFolder")
    public Message updateTagFolder(@RequestBody UpdateTagFolderDTO tagFolderDTO)
    {
        tagFolderService.updateTagFolder(tagFolderDTO);
        return Message.createSuc("更新成功");
    }

    @GetMapping("tag")
    public Message getTagList(@RequestParam String tagFolderId)
    {
        return Message.createSuc(tagService.getTagList(tagFolderId));
    }

    @GetMapping("tagFolder")
    public Message getTagList()
    {
        return Message.createSuc(tagFolderService.getTagFolderList());
    }

}
