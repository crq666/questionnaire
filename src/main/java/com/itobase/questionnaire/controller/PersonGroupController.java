package com.itobase.questionnaire.controller;

import com.itobase.questionnaire.model.PersonGroup;
import com.itobase.questionnaire.service.IPersonGroupService;
import com.itobase.questionnaire.util.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author: lphu
 * @create: 2018-08-31 14:51
 * @description:
 */
@RestController
@RequestMapping("personGroup")
public class PersonGroupController
{
    @Autowired
    IPersonGroupService personGroupService;

    @PostMapping("add")
    public Message addGroup(@RequestBody PersonGroup personGroup)
    {
        return Message.createSuc(personGroupService.addGroup(personGroup));
    }

    @GetMapping("get")
    public Message getGroup()
    {
        return Message.createSuc(personGroupService.getGroup());
    }
}
