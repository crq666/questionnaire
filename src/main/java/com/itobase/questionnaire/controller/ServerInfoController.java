package com.itobase.questionnaire.controller;

import com.itobase.questionnaire.util.Message;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.net.URLEncoder;

/**
 * @author liucong
 * @date 2018/12/6
 */
@RestController
@RequestMapping("server")
public class ServerInfoController {

    @GetMapping("/sessionCount")
    public Message sessionCount(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse){
        HttpSession session = httpServletRequest.getSession();
        Object count=session.getServletContext().getAttribute("count");
        return Message.createSuc("当前session数量:"+count);
    }

}
