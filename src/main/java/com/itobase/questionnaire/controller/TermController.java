package com.itobase.questionnaire.controller;

import com.itobase.questionnaire.model.Term;
import com.itobase.questionnaire.service.ITermService;
import com.itobase.questionnaire.util.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: lphu
 * @create: 2018-11-11 14:26
 * @description:
 */

@RestController
@RequestMapping("term")
public class TermController
{
    @Autowired
    ITermService termService;

    @PostMapping("add")
    public Message addTerm(Term term)
    {
        return Message.createSuc("");
    }
}
