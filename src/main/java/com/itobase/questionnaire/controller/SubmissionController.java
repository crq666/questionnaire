package com.itobase.questionnaire.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.itobase.questionnaire.model.Submission;
import com.itobase.questionnaire.model.Survey;
import com.itobase.questionnaire.model.auth.User;
import com.itobase.questionnaire.repository.SubmitRepository;
import com.itobase.questionnaire.repository.SurveyRepository;
import com.itobase.questionnaire.repository.UserRepository;
import com.itobase.questionnaire.service.ISubmitService;
import com.itobase.questionnaire.util.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Optional;

/**
 * 处理提交的已经填写的问卷
 *
 * @author cong
 * @date 2018/7/29
 */
@RestController
@RequestMapping("submit")
public class SubmissionController {

    @Autowired
    SubmitRepository submitRepository;


    @Autowired
    ISubmitService submitService;


    @Autowired
    SurveyRepository surveyRepository;

    /**
     * 获取当前登录人的所有提交
     * @return
     */
    @GetMapping
    public Message getAllSubitByPerson(){
        //已完成
        return null;
    }

    @PutMapping
    public Message addSubmit(@RequestBody String jsonString, HttpServletRequest request) throws Throwable {
        Submission submission = JSON.parseObject(jsonString, new TypeReference<Submission>() {
        });


        return submitService.addSubmission(submission, request);
    }

    /**
     * 获取提交的答卷的数量
     * @param surveyId
     * @return
     */
    @GetMapping("count")
    public Message getSubmitCount(@RequestParam String surveyId){

        return Message.createSuc(submitService.getSubmitsCount(surveyId));
    }

    /**
     * 获取应有答卷的数量
     * @param surveyId
     * @return
     */
    @GetMapping("totalCount")
    public Message getShouldSubmit(@RequestParam String surveyId){
        Survey survey = Optional.of(surveyRepository.findOne(surveyId)).orElseThrow(()->new RuntimeException("调查id错误"));
        return Message.createSuc(survey.getUserList().size());
    }

    /**
     * 获取完成率
     * @return
     */
    @GetMapping("completeRate")
    public Message getCompleteRate(@RequestParam String surveyId){
        Survey survey = Optional.ofNullable(surveyRepository.findOne(surveyId)).orElseThrow(()->new RuntimeException("调查id错误"));
        List<User> userList = Optional.ofNullable(survey.getUserList()).orElseThrow(()->new RuntimeException("调查未绑定调查组"));
        DecimalFormat decimalFormat = new DecimalFormat("0.00");
        Integer submitCount = submitService.getSubmitsCount(surveyId);
        double completeRate = (submitCount*1.00)/userList.size();
        return Message.createSuc(decimalFormat.format(completeRate));
    }

    /**
     * 获取调查的类型组以及完成数量
     * @param surveyId
     * @return
     */
    @GetMapping("typeCount")
    public Message getTypeCompleteCount(@RequestParam String surveyId){
        Survey survey = Optional.of(surveyRepository.findOne(surveyId)).orElseThrow(()->new RuntimeException("调查不存在"));
        return Message.createSuc(submitService.getTypeCompleteCount(survey));
    }


}
