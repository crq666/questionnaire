package com.itobase.questionnaire.controller;

import com.itobase.questionnaire.VO.PageDatas;
import com.itobase.questionnaire.model.Questionnaire;
import com.itobase.questionnaire.repository.QuesnaireRepository;
import com.itobase.questionnaire.service.IExcelService;
import com.itobase.questionnaire.service.impl.QuesnaireService;
import com.itobase.questionnaire.util.Message;
import com.itobase.questionnaire.util.PagingUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

/**
 * @author liucong
 * @date 2018/8/4
 */
@RestController
public class QuesnaireController {

    @Autowired
    QuesnaireService quesnaireService;

    @Autowired
    QuesnaireRepository quesnaireRepository;

    @Autowired
    IExcelService excelService;

    //设置问卷为失效
    @PutMapping("invalid")
    public Message invalid(@RequestParam String id) {
        Questionnaire questionnaire = quesnaireService.findById(id);
        if(questionnaire==null){
            return Message.createSuc("问卷id出错，未找到该问卷");
        }else{
            questionnaire.setExpired(true);
            return Message.createSuc(quesnaireService.update(questionnaire));
        }
    }

    //查看问卷详情即预览问卷
    @GetMapping("detail")
    public Message detail(@RequestParam String id){
        Questionnaire questionnaire = quesnaireRepository.findOne(id);
        if(questionnaire==null){
            return Message.createSuc("问卷id出错，未找到该问卷");
        }else {
            List<String> list = new ArrayList<>();
            list.add(questionnaire.getName());
            list.add(questionnaire.getXmlStr());
            return Message.createSuc(list);
        }
    }

    //删除问卷
    @DeleteMapping("delete")
    public Message delete(@RequestParam String id) {
        Questionnaire questionnaire = quesnaireService.findById(id);
        if(questionnaire==null){
            return Message.createSuc("问卷id出错，未找到该问卷");
        }else {
            if (questionnaire.getUsed() == false) {
                quesnaireService.delete(id);
                return Message.createSuc("问卷删除成功");
            } else {
                return Message.createSuc("问卷已绑定调查，不能删除");
            }
        }
    }

    //根据关键词筛选问卷
    @GetMapping("select")
    public Message search_questionnaire(@PageableDefault(page =1) Pageable pageable, @RequestParam(required = false) String word) {
        List<Questionnaire> list = quesnaireRepository.findAll(new Sort(Sort.Direction.DESC,"createTime"));
        List<Questionnaire> list_new = new ArrayList<>();
        for (int i =0; i<list.size(); i++) {
            if(list.get(i).getName()!=null) {
                if (list.get(i).getName().indexOf(word) != -1) {
                    list_new.add(list.get(i));
                }
            }
        }
        PageDatas<Questionnaire> pageDatas = PagingUtils.paging(pageable.getPageNumber(),list_new,pageable.getPageSize());
        return Message.createSuc(pageDatas);
    }

    //获取问卷列表
    @GetMapping("list")
    public Message questionnaire_list(@PageableDefault(page =1)Pageable pageable){
        List<Questionnaire> list = quesnaireRepository.findAll(new Sort(Sort.Direction.DESC,"createTime"));
        if(list.isEmpty() ) {
            return Message.createSuc("问卷列表为空");
        }else{
            PageDatas<Questionnaire> pageDatas = PagingUtils.paging(pageable.getPageNumber(), list, pageable.getPageSize());
            return Message.createSuc(pageDatas);
        }
    }

    //更新问卷
    @PostMapping("update")
    public Message questionnaire_update(@RequestParam MultipartFile file,@RequestParam String id)throws Exception{
        Questionnaire questionnaire = quesnaireRepository.findById(id);
        if(questionnaire==null){
            return Message.createSuc("问卷id出错，未找到该问卷");
        }else{
            if (questionnaire.getUsed() == false) {
                List<String> list = excelService.upLoad(file);
                questionnaire.setXmlStr(list.get(1));
                quesnaireRepository.delete(list.get(0));
                return Message.createSuc("更新成功");
            } else {
                return Message.createSuc("该问卷已绑定调查，不能更新");
            }
        }
    }

}
