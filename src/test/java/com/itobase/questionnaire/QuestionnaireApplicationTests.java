package com.itobase.questionnaire;

import com.itobase.questionnaire.model.Question;
import com.itobase.questionnaire.model.Questionnaire;
import com.itobase.questionnaire.model.Submission;
import com.itobase.questionnaire.model.auth.User;
import com.itobase.questionnaire.repository.UserRepository;
import com.itobase.questionnaire.service.IQuesnaireService;
import com.itobase.questionnaire.service.impl.QuesnaireService;
import com.itobase.questionnaire.util.AuthUtil;
import com.itobase.questionnaire.util.DateUtil;

import com.mongodb.gridfs.GridFSDBFile;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.List;


@RunWith(SpringRunner.class)
@SpringBootTest
@EnableCaching(proxyTargetClass = true)
public class QuestionnaireApplicationTests {

    @Autowired
    GridFsTemplate gridFsTemplate;

    @Autowired
    UserRepository userRepository;

    @Autowired
    IQuesnaireService quesnaireService;

    @Test
    public void contextLoads() {
    }

    @Test
    public void testFindInArray(){
        String[] a = new String[]{"5bceb48ae18ae14f043a1c76","5bceb48ae18ae14f043a1c76","5bceb48ae18ae14f043a1c76"};
        List<User> userList = userRepository.findByIdIn(a);
        System.out.println(userList.size());
    }

    @Test
    public void testGridFs() throws IOException {
        /*File file = new File("/Users/admin/Desktop/1602080203-软件1601-刘聪.md");
        InputStream inputStream = new FileInputStream(file);
        GridFSFile gridFSFile = gridFsTemplate.store(inputStream,file.getName());*/

        GridFSDBFile gridFSDBFile = gridFsTemplate.findOne(new Query(Criteria.where("_id").is("5b5daa26f8128675628bcdd9")));
        System.out.println("ContentType" + gridFSDBFile.getContentType());
        InputStreamReader inputStreamReader = new InputStreamReader(gridFSDBFile.getInputStream());
        BufferedReader reader = new BufferedReader(inputStreamReader);
        StringBuilder txt = new StringBuilder();
        String line = null;
        while ((line = reader.readLine()) != null) {
            txt.append(line);
        }
        System.out.println("inputStream" + txt);
        inputStreamReader.close();
        reader.close();
        System.out.println(gridFSDBFile.keySet());
    }

    @Test
    public void testCache() {
        Questionnaire questionnaire = quesnaireService.findById("5b5fffef0070d33484c87e6b");
        System.out.println(questionnaire.getQuestionList());
        Questionnaire questionnaire1 = quesnaireService.findById("5b5fffef0070d33484c87e6b");
        quesnaireService.delete("5b5fffef0070d33484c87e6b");

        System.out.println("secong" + questionnaire1.getId());
    }

    @Test
    public void testSubmit() throws Throwable {
        Submission submission = new Submission();
        submission.setSubmitTime(DateUtil.format(new Date()));
        submission.setUserId(AuthUtil.getUserId());


    }




}
