//package com.itobase.questionnaire.utilTest;
//
//import com.alibaba.fastjson.JSON;
//import com.itobase.questionnaire.VO.CountCompleteType;
//import com.itobase.questionnaire.model.EnumList.QuestionTypes;
//import com.itobase.questionnaire.model.Question;
//import com.itobase.questionnaire.model.Submission;
//import com.itobase.questionnaire.model.Teacher;
//import com.itobase.questionnaire.model.auth.User;
//import com.itobase.questionnaire.util.DateUtil;
//import org.junit.Test;
//import org.springframework.util.ResourceUtils;
//
//import java.io.File;
//import java.io.FileNotFoundException;
//import java.text.MessageFormat;
//import java.text.ParseException;
//import java.util.*;
//
///**
// * @author cong
// * @date 2018/7/28
// */
//public class EnumTest {
//
//    @Test
//    public void testEnum() {
//        QuestionTypes type = QuestionTypes.Single;
//        if (type.equals(QuestionTypes.Single)) {
//            System.out.println("true");
//        }
//        List<User> userList = new ArrayList<>();
//        User user = new User();
//        user.setUsername("1");
//        user.setPassword("2");
//        userList.add(user);
//        user = new User();
//        user.setUsername("3");
//        user.setPassword("4");
//        userList.add(user);
//        System.out.println(JSON.toJSONString(userList));
//    }
//
//    @Test
//    public void testList(){
//        List<String> stringList = new ArrayList<>();
//        stringList.add("23");
//        stringList.add("22");
//        stringList.add("22");
//        System.out.println(stringList);
//        System.out.println(stringList.subList(0,2));
//        System.out.println(stringList.size());
//        System.out.println(stringList.get(1));
//    }
//
//    @Test
//    public void testDate(){
//        System.out.println(MessageFormat.format("(总满意度：{0})", "123"));
//        Properties properties = System.getProperties();
//        System.out.println("操作系统的名称：" + properties.getProperty("os.name"));
//        System.out.println(properties.getProperty("os.name").contains("Mac OS X"));
//        List<String> stringList = new LinkedList<>();
//        stringList.add("df");
//        stringList.add("233");
//        stringList.add("w354r");
//        System.out.println(stringList.get(0));
//        System.out.println(stringList.get(1));
//    }
//
//    @Test
//    public void testValueOfString(){
//        System.out.println(String.valueOf(2.3));
//    }
//
//    @Test
//    public void testSortQues(){
//        List<Question> questions = new ArrayList<>();
//        Question question = new Question();
//        question.setOrderNumber(1);
//        questions.add(question);
//        question = new Question();
//        question.setOrderNumber(2);
//        questions.add(question);
//        questions.get(0).setOrderNumber(10);
//        Collections.sort(questions);
//        System.out.println(questions);
//
//    }
//    @Test
//    public void testHashSet(){
//        Set<String> set = new HashSet<>();
//        set.add("123");
//        set.add("456");
//        String value = "A.单选题";
//        value = value.substring(0,value.indexOf("."));
//        System.out.println(value);
//        System.out.println(set.contains("123"));
//    }
//
//
//    @Test
//    public void testResources() throws FileNotFoundException {
//        File path = new File(ResourceUtils.getURL("classpath:").getPath());
//        String gitPath= path.getParentFile().getParentFile().getParent() + File.separator + "html" + File.separator + "uploads" + File.separator;
//
//        System.out.println(gitPath);
//        System.out.println(path.getParentFile().getParentFile().getParent());
//    }
//
//
//    @Test
//    public void testJson() {
//        Map<Integer, String> map = new HashMap<>();
//        Submission submission = new Submission();
//        submission.setSurveyId("5b67f924cf4d6957804a428e");
//        map.put(1, "刘聪");
//        map.put(2, "A");
//        map.put(3, "C");
//        map.put(4, "A");
//        map.put(5, "A");
//        map.put(6, "A");
//        map.put(7, "A");
//        map.put(8, "A");
//        map.put(9, "A");
//        map.put(10, "我认为有待改善");
//        submission.setAnswer(map);
//        System.out.println(map.size());
//        String jsonString = JSON.toJSONString(submission);
//        System.out.println(jsonString);
//    }
//
//    @Test
//    public void testJson1(){
//        Map<String,List<String>> map = new HashMap<>();
//        List<String> list = new ArrayList<>();
//        list.add("1");
//        list.add("2");
//        map.put("1",list);
//        String str = com.alibaba.fastjson.JSON.toJSONString(map);
//        System.out.println(str);
//
//        CountCompleteType countCompleteType = new CountCompleteType();
//        countCompleteType.setGroupName("test");
//        Map<String,Integer> map1 = new HashMap<>();
//        map1.put("办公室主任",2);
//        countCompleteType.setTypeCount(map1);
//        System.out.println(JSON.toJSONString(countCompleteType));
//    }
//
//    @Test
//    public void testArray(){
//        Map<String,String> map = new HashMap<>();
//        map.put("a","1");
//        System.out.println(map.get("b"));
//        User user = new User();
//        user.setUsername(null);
//        System.out.println(user);
//    }
//
//
//    @Test
//    public void testInstance(){
//        User user = new Teacher();
//        if(user.getClass().isAssignableFrom(Teacher.class)){
//            System.out.println(true);
//        }
//
//    }
//
//    @Test
//    public void testDateCurrent() throws ParseException {
//        Date date = new Date();
//        Date date1 = DateUtil.parse_day("2018-11-01");
//        System.out.println(date.compareTo(date1));
//    }
//}
