//package com.itobase.questionnaire.utilTest;
//
//import com.fasterxml.jackson.databind.deser.BeanDeserializerFactory;
//import com.fasterxml.jackson.databind.ser.BeanSerializerFactory;
//import com.itobase.questionnaire.util.DateUtil;
//import com.itobase.questionnaire.util.MD5Util;
//import org.apache.commons.codec.digest.DigestUtils;
//import org.apache.http.*;
//import org.apache.http.client.HttpClient;
//import org.apache.http.client.entity.UrlEncodedFormEntity;
//import org.apache.http.client.methods.CloseableHttpResponse;
//import org.apache.http.client.methods.HttpGet;
//import org.apache.http.client.methods.HttpPost;
//import org.apache.http.entity.StringEntity;
//import org.apache.http.impl.client.CloseableHttpClient;
//import org.apache.http.impl.client.DefaultHttpClient;
//import org.apache.http.impl.client.HttpClients;
//import org.apache.http.message.BasicNameValuePair;
//import org.apache.http.util.EntityUtils;
//import org.junit.Test;
//
//import java.io.BufferedReader;
//import java.io.IOException;
//import java.io.InputStreamReader;
//import java.util.*;
//
//import static org.springframework.http.HttpHeaders.USER_AGENT;
//
///**
// * @author liucong
// * @date 2018/8/6
// */
//public class UrlTest {
//
//
//
//
//    @Test
//    public void testLongUrlTOShort() {
//
//        String[] chars = new String[]{"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p",
//                "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "0", "1", "2", "3", "4", "5",
//                "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "G", "H",
//                "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T",
//                "U", "V", "W", "X", "Y", "Z"};
//
//        String longUrl = "5b936dbf0070d35eec368fec";
//        String hex = MD5Util.encode(longUrl);
//
//        String[] resUrl = new String[4];
//        for (int i = 0; i < 4; i++) {
//            // 把加密字符按照 8 位一组 16 进制与 0x3FFFFFFF 进行位与运算
//            String sTempSubString = hex.substring(i * 8, i * 8 + 8);
//            // 这里需要使用 long 型来转换，因为 Inteper .parseInt() 只能处理 31 位 , 首位为符号位 , 如果不用
//            // long ，则会越界
//            long lHexLong = 0x3FFFFFFF & Long.parseLong(sTempSubString, 16);
//            String outChars = "";
//            for (int j = 0; j < 6; j++) {
//                // 把得到的值与 0x0000003D 进行位与运算，取得字符数组 chars 索引
//                long index = 0x0000003D & lHexLong;
//                // 把取得的字符相加
//                outChars += chars[(int) index];
//                // 每次循环按位右移 5 位
//                lHexLong = lHexLong >> 5;
//            }
//
//            // 把字符串存入对应索引的输出数组
//            resUrl[i] = outChars;
//        }
//        System.out.println(Arrays.toString(resUrl));
//        int order = new Random().nextInt(4);
//        System.out.println(resUrl[order]);
//    }
//
//
//
//    @Test
//    public void testSendMessage() throws IOException {
//        Calendar calendar = Calendar.getInstance();
//        calendar.set(Calendar.MINUTE,calendar.get(Calendar.MINUTE)+5);
//        Date sendTime = calendar.getTime();
//        System.out.println(DateUtil.format(sendTime));
//
//
//
//    }
//    @Test
//    public void testSendBySchool(){
//        String url = "http://221.131.216.59/sms/services/GaoxtSMSInterfaceForSchools?wsdl";
//        Calendar calendar = Calendar.getInstance();
//        calendar.set(Calendar.MINUTE,calendar.get(Calendar.MINUTE)+5);
//        Date sendTime = calendar.getTime();
//
//        Map<String,String> map = new HashMap<>();
//        map.put("loginName","zjgsu-fzghc");
//        map.put("passWord","28877526qym ");
//        map.put("schoolCode","10353");
//        map.put("smsMsg","验证码654321");
//        map.put("sendTime", DateUtil.format(sendTime));
//        map.put("mpNumber","15355436356");
//        map.put("userDefId","0");
//
//        CloseableHttpClient httpclient = HttpClients.createDefault();
//        List<NameValuePair> formparams = new ArrayList<>();
//        for (Map.Entry<String, String> entry : map.entrySet()) {
//            //给参数赋值
//            formparams.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
//        }
//        UrlEncodedFormEntity entity = new UrlEncodedFormEntity(formparams, Consts.UTF_8);
//        HttpPost httppost = new HttpPost(url);
//        httppost.setEntity(entity);
//        CloseableHttpResponse response = null;
//        try {
//            response = httpclient.execute(httppost);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        HttpEntity resultEntity = (response != null ? response.getEntity() : null);
//        String result = null;
//        try {
//            result = EntityUtils.toString(resultEntity);
//        } catch (ParseException | IOException e) {
//            e.printStackTrace();
//        }
//        assert result != null;
//        if(Integer.parseInt(result)==0){
//            System.out.println("发送成功");
//        }
//
//    }
//
//
//    @Test
//    public void testPostSend() {
//        List<String> phones = new ArrayList<>();
//        phones.add("15355436356");
//        phones.add("18110703370");
//        String phoneStr = phones.toString();
//        String url = "http://utf8.api.smschinese.cn/";
//        Map<String, String> map = new HashMap<>();
//
//        map.put("Uid", "congJava");
//        map.put("Key", "d41d8cd98f00b204e980");
//        map.put("smsMob", phoneStr.substring(phoneStr.indexOf("[") + 1, phoneStr.lastIndexOf("]")));
//        map.put("smsText", "验证码：123456");
//
//
//        CloseableHttpClient httpclient = HttpClients.createDefault();
//        List<NameValuePair> formparams = new ArrayList<>();
//        for (Map.Entry<String, String> entry : map.entrySet()) {
//            //给参数赋值
//            formparams.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
//        }
//        UrlEncodedFormEntity entity = new UrlEncodedFormEntity(formparams, Consts.UTF_8);
//        HttpPost httppost = new HttpPost(url);
//        httppost.setEntity(entity);
//        CloseableHttpResponse response = null;
//        try {
//            response = httpclient.execute(httppost);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        HttpEntity resultEntity = (response != null ? response.getEntity() : null);
//        String result = null;
//        try {
//            result = EntityUtils.toString(resultEntity);
//        } catch (ParseException | IOException e) {
//            e.printStackTrace();
//        }
//        System.out.println("成功发送了" + result + "条信息");
//    }
//
//
////    @Test
////    public void testSchoolSend(){
////        String wsdlUrl = "http:// 221.131.216.59/sms/services/GaoxtSMSInterfaceForSchools?wsdl";
////        String nameSpaceUri = "http:// 221.131.216.59/sms/services/GaoxtSMSInterfaceForSchools";
////        // 创建调用对象
////        Service service = new Service();
////        Call call = (Call)service.createCall();
////        QName qn = new QName("http:// 221.131.216.59/sms/services/GaoxtSMSInterfaceForSchools","moitem");
////        call.registerTypeMapping(MOItem.class, qn, new BeanSerializerFactory(MOItem.class,qn), new BeanDeserializerFactory(MOItem.class,qn));
////        // 调用getMessage
////        call.setOperationName(new QName(nameSpaceUri, "sendSMS"));
////        call.setTargetEndpointAddress(new java.net.URL(wsdlUrl));
////        //int ret = (Integer)call.invoke(new Object[] {});
////        String loginName = "";
////        String passWord = "";
////        String schoolCode = "";
////        String smsMsg = "短信接口测试20";
////        String sendTime = "2010-12-17 15:44:39";
////        String mps = "";
////        long userDefId = 2;
////        //MOItem[] MOItems = (MOItem[]) call.invoke(new Object[]{
////        //loginName,passWord,schoolCode});
////        Object ret = (Object)call.invoke(new Object[] {
////                loginName,passWord,schoolCode,smsMsg,sendTime,mps,userDefId});
////        System.out.println("return value is"+ret.toString());
////
////    }
//
//
//    @Test
//    public void testSendJson() throws IOException {
//        //发送请求的URL
//
//        String url = "http://www.baidu.com";
//
////编码格式
//
//        String charset = "UTF-8";
//
////请求内容
//
//        String content = "";
////使用帮助类HttpClients创建CloseableHttpClient对象.
//
//        CloseableHttpClient client = HttpClients.createDefault();
//
////HTTP请求类型创建HttpPost实例
//
//        HttpPost post = new HttpPost(url);
//
////使用addHeader方法添加请求头部,诸如User-Agent, Accept-Encoding等参数.
//
//        post.setHeader("Content-Type", "application/json;charset=UTF-8");
//
////组织数据
//        StringEntity stringEntity = new StringEntity(content);
//
//        stringEntity.setContentEncoding(charset);
//
//        stringEntity.setContentType("application/json");
//
//        post.setEntity(stringEntity);
//
//        CloseableHttpResponse response = client.execute(post);
//
//
//        HttpEntity responseEntity = response.getEntity();
//
//        String resData = EntityUtils.toString(responseEntity);
//        System.out.println(resData);
//
//    }
//
//    @Test
//    public void urlTest(){
//        String url = "http://10.11.125.124:8080/questionnaire/q/qq6jAr";
//        String newUrl = url.substring(7);
//        System.out.println(newUrl);
//
//    }
//}
