package com.itobase.questionnaire.utilTest;

import com.itobase.questionnaire.QuestionnaireApplication;
import com.itobase.questionnaire.QuestionnaireApplicationTests;
import com.itobase.questionnaire.model.EnumList.MOItem;
import com.itobase.questionnaire.model.SendPool;
import com.itobase.questionnaire.model.auth.User;
import com.itobase.questionnaire.repository.SendRepository;
import com.itobase.questionnaire.repository.UserRepository;
import com.itobase.questionnaire.service.IUserService;
import com.itobase.questionnaire.service.impl.UrlService;
import com.itobase.questionnaire.service.impl.UserService;
import com.itobase.questionnaire.template.Constant;
import com.itobase.questionnaire.util.MD5Util;
import com.sun.org.apache.xerces.internal.impl.XMLDocumentScannerImpl;
import javafx.application.Application;
import org.apache.axis.client.Call;
import org.apache.axis.client.Service;
import org.apache.axis.encoding.ser.BeanDeserializerFactory;
import org.apache.axis.encoding.ser.BeanSerializerFactory;
import org.apache.http.client.HttpClient;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.assertj.core.util.Maps;
import org.dom4j.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.data.mongodb.core.aggregation.ArrayOperators;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.xml.namespace.QName;
import java.io.*;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.*;


/**
 * @Description TODO
 * @Author 陈荣锵
 * @Date 2019/10/19 20:24
 * @Version 1.0
 **/
@RunWith(SpringRunner.class)
@SpringBootTest
public class importTest {
    @Autowired
    UrlService urlService;
    @Autowired
    UserRepository userRepository;
    @Autowired
    SendRepository sendRepository;
//    @Autowired
//    IUserService userService;
//    @Autowired
//    UserRepository userRepository;
//    @Test
//    //3993
//    public void exportStudent() {
//        String path = "d:/人员名单/本科生名册.xls";
//        InputStream is = null;//将路径转为输入流对象 输入流对象可以取出来读取
//        try {
//            is = new FileInputStream(path);
//            HSSFWorkbook hssfWorkbook = new HSSFWorkbook(is);//将输入流对象存到工作簿对象里面
//            HSSFSheet sheet = hssfWorkbook.getSheet("sheet1");
//            int count=0;
////            rowNum <= sheet.getLastRowNum()
//            for (int rowNum = 1; rowNum <= sheet.getLastRowNum(); rowNum++) {
//                HSSFRow hssfRow = sheet.getRow(rowNum);
//                if (hssfRow == null) {
//                    continue;
//                }
//                String username=new BigDecimal(String.valueOf(hssfRow.getCell(0).getNumericCellValue())).toPlainString();
//                User user1=userRepository.findByUsername(username);
//                if(null==user1){
//                    //System.out.println(userRepository.findByUsername(username));
//                    User user=new User();
//                    user.setUserTypeId("5b88e7a5dc8b5a1a84a96367");
//                    user.setName(hssfRow.getCell(1).getStringCellValue());
//                    user.setClassName(hssfRow.getCell(4).getStringCellValue());
//                    String longPhone=new BigDecimal(String.valueOf(hssfRow.getCell(5).getNumericCellValue())).toPlainString();
//                    user.setLongPhone(longPhone);
//                    user.setPassword(MD5Util.encode(user.getLongPhone()));
//                    user.setCollegeName(hssfRow.getCell(3).getStringCellValue());
//                    user.setGender(hssfRow.getCell(2).getStringCellValue());
//                    user.setUsername(username);
//                    user.setFlag(true);
//                    userRepository.insert(user);
//                    System.out.println(username);
//                    count++;
//                }
//                }
//            System.out.println(count);
//            while(true);
//        }catch(FileNotFoundException e){
//            e.printStackTrace();
//        } catch(IOException e){
//            e.printStackTrace();
//        }
//    }
//    @Test
//    //723
//    public void importPostGrat(){
//        String path = "d:/人员名单/研究生信息.xls";
//        InputStream is = null;//将路径转为输入流对象 输入流对象可以取出来读取
//        try {
//            is = new FileInputStream(path);
//            HSSFWorkbook hssfWorkbook = new HSSFWorkbook(is);//将输入流对象存到工作簿对象里面
//            HSSFSheet sheet = hssfWorkbook.getSheet("sheet0");
//            int count=0;
//
//            for (int rowNum = 1; rowNum <= sheet.getLastRowNum(); rowNum++) {
//                HSSFRow hssfRow = sheet.getRow(rowNum);
//                if (hssfRow == null) {
//                    continue;
//                }
//                String username=hssfRow.getCell(0).getStringCellValue();
//                User user1=userRepository.findByUsername(username);
//                if(null==userRepository.findByUsername(hssfRow.getCell(0).getStringCellValue())){
//                    User user=new User();
//                    user.setUserTypeId("5b88e7e8dc8b5a1a84a96370");
//                    user.setName(hssfRow.getCell(1).getStringCellValue());
//                    String longPhone=hssfRow.getCell(3).getStringCellValue();
//                    user.setLongPhone(longPhone);
//                    user.setPassword(MD5Util.encode(user.getLongPhone()));
//                    user.setCollegeName(hssfRow.getCell(2).getStringCellValue());
//                    user.setUsername(hssfRow.getCell(0).getStringCellValue());
//                    user.setLevel("硕士研究生");
//                    user.setFlag(true);
//                    userRepository.insert(user);
//                    System.out.println(user.getName());
//                    count++;
//                }
//            }
//            System.out.println(count);
//            while(true);
//        }catch(FileNotFoundException e){
//            e.printStackTrace();
//        } catch(IOException e){
//            e.printStackTrace();
//        }
//
//    }
//    @Test
//    public void test(){
//       User user=new User();
//       user.setLongPhone("123456789");
//        user.setPassword(MD5Util.encode(user.getLongPhone()));
//        user.setUsername("123456");
//        userRepository.insert(user);
//    }
//    @Test
//    public void importTeacher(){
//        String path = "d:/人员名单/教师名单.xls";
//        InputStream is = null;//将路径转为输入流对象 输入流对象可以取出来读取
//        try {
//            is = new FileInputStream(path);
//            HSSFWorkbook hssfWorkbook = new HSSFWorkbook(is);//将输入流对象存到工作簿对象里面
//            HSSFSheet sheet = hssfWorkbook.getSheet("sheet1");
//            int count=0;
//
//            for (int rowNum = 1; rowNum <= sheet.getLastRowNum(); rowNum++) {
//                HSSFRow hssfRow = sheet.getRow(rowNum);
//                if (hssfRow == null) {
//                    continue;
//                }
//                hssfRow.getCell(1).setCellType(CellType.STRING);
//                String username=hssfRow.getCell(1).getStringCellValue();
//               // username=username.substring(0,username.lastIndexOf("."));
//                //System.out.println(username);
//                User user1=userRepository.findByUsername(username);
//                if(null==user1){
//                    User user=new User();
//                    user.setUserTypeId("5b88e7e8dc8b5a1a84a96368");
//                    user.setName(hssfRow.getCell(2).getStringCellValue());
//                    hssfRow.getCell(3).setCellType(CellType.STRING);
//                    String longPhone=hssfRow.getCell(3).getStringCellValue();
//                    user.setLongPhone(longPhone);
//                    user.setPassword(MD5Util.encode(user.getLongPhone()));
//                    user.setCollegeName(hssfRow.getCell(4).getStringCellValue());
//                    user.setUsername(username);
//                    user.setFlag(true);
//                    userRepository.insert(user);
//                    System.out.println(username);
//                    count++;
//                }
//            }
//            System.out.println(count);
//            while(true);
//        }catch(FileNotFoundException e){
//            e.printStackTrace();
//        } catch(IOException e){
//            e.printStackTrace();
//        }
//    }
//    @Test
//    public void testSend() throws MalformedURLException, ProtocolException, DocumentException {
//
//        try {
//
//            HttpURLConnection connection=(HttpURLConnection)new URL("http://115.236.101.37:8181/gxtMsgService/services/SendMsg").openConnection();
//            connection.setDoOutput(true);
//            connection.setDoInput(true);
//            connection.setRequestMethod("POST");
//            connection.setRequestProperty("Content-Type","text/xml;charset=UTF-8");
//            connection.setRequestProperty("soapaction","");
//            connection.connect();//连接服务器
//            OutputStream os=connection.getOutputStream();
//            String xml="<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ser=\"http://service.pubinfo.com.cn\">\n" +
//                    "   <soapenv:Header/>\n" +
//                    "   <soapenv:Body>\n" +
//                    "      <ser:sendMsg>\n" +
//                    "         <ser:in0>28877777</ser:in0>\n" +
//                    "         <ser:in1>9f8cc234648df1d133c83679302df1a3</ser:in1>\n" +
//                    "         <ser:in2>17376569894</ser:in2>\n" +
//                    "         <ser:in3>123456</ser:in3>\n" +
//                    "      </ser:sendMsg>\n" +
//                    "   </soapenv:Body>\n" +
//                    "</soapenv:Envelope>";
//            String test="<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><soap:Body><ns1:sendMsgResponse xmlns:ns1=\"http://service.pubinfo.com.cn\"><ns1:out>00000</ns1:out></ns1:sendMsgResponse></soap:Body></soap:Envelope>";
//            os.write(xml.getBytes());
//            os.flush();
//            os.close();
//            InputStream is = connection.getInputStream();//获取数据，真正的请求从此处开始
//            byte[] bts=new byte[is.available()];
//            is.read(bts);
//            Document doc= DocumentHelper.parseText(test);
//           // System.out.println(new String(bts));
//            Element rootElt = doc.getRootElement(); // 获取根节点
//            Map<String,String> map = new HashMap();
//            map.put("soap",rootElt.getNamespaceURI());
//            map.put("ns1","http://service.pubinfo.com.cn");
//            System.out.println(rootElt.getNamespaceURI());
//            XPath xPath = doc.createXPath("//soap:Body//ns1:sendMsgResponse//ns1:out");
//            xPath.setNamespaceURIs(map);
//            Node node = xPath.selectSingleNode(doc);
//            System.out.println(node.getText());
//            try{
//                Thread.sleep(100000);
//            }catch(Exception e){
//                System.exit(0);//退出程序
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        catch (DocumentException e) {
//            e.printStackTrace();
//        }
//
//    }
//    @Test
//    public void testInsert(){
//        String path = "d:/人员名单/sh.xlsx";
//        InputStream is = null;//将路径转为输入流对象 输入流对象可以取出来读取
//        try {
//            is = new FileInputStream(path);
//            XSSFWorkbook xssfWorkbook = new XSSFWorkbook(is);//将输入流对象存到工作簿对象里面
//            XSSFSheet sheet = xssfWorkbook.getSheet("sheet1");
//            int count=90;
//            for (int rowNum = 1; rowNum <= sheet.getLastRowNum(); rowNum++) {
//                XSSFRow xssfRow = sheet.getRow(rowNum);
//                if (xssfRow == null) {
//                    continue;
//                }
//
//                //xssfRow.getCell(1).setCellType(CellType.STRING);
//                Double numPhone=xssfRow.getCell(3).getNumericCellValue();
//               // username=username.substring(0,username.lastIndexOf("."));
//                //System.out.println(username);
//                String longPhone=new BigDecimal(numPhone.toString()).toPlainString();
//                User user1=userRepository.findByLongPhone(longPhone);
//                if(null==user1){
//                    User user=new User();
//                    user.setUserTypeId("5b88e7a5dc8b5a1a84a96367");
//                    user.setName(xssfRow.getCell(2).getStringCellValue());
//                    user.setLongPhone(longPhone);
//                    user.setPassword(MD5Util.encode(user.getLongPhone()));
//                    user.setCollegeName("国教学院");
//                    String username=null;
//                    if(count<100)
//                    username="sh0"+count;
//                    else
//                        username="sh"+count;
//                    user.setUsername(username);
//                    user.setFlag(true);
//                    userRepository.insert(user);
//                    System.out.println(username);
//                    count++;
//                }
//            }
//            System.out.println(count);
//        }catch(FileNotFoundException e){
//            e.printStackTrace();
//        } catch(IOException e){
//            e.printStackTrace();
//        }
//    }
//    @Test
//    public void testInsert1(){
//        String path = "d:/人员名单/jg.xlsx";
//        InputStream is = null;//将路径转为输入流对象 输入流对象可以取出来读取
//        try {
//            is = new FileInputStream(path);
//            XSSFWorkbook xssfWorkbook = new XSSFWorkbook(is);//将输入流对象存到工作簿对象里面
//            XSSFSheet sheet = xssfWorkbook.getSheet("sheet2");
//            int count=83;
//            for (int rowNum = 1; rowNum <= sheet.getLastRowNum(); rowNum++) {
//                XSSFRow xssfRow = sheet.getRow(rowNum);
//                if (xssfRow == null) {
//                    continue;
//                }
//
//                xssfRow.getCell(3).setCellType(CellType.STRING);
//                String longPhone=xssfRow.getCell(3).getStringCellValue();
//                // username=username.substring(0,username.lastIndexOf("."));
//                //System.out.println(username);
//                //String longPhone=new BigDecimal(numPhone.toString()).toPlainString();
//                User user1=userRepository.findByLongPhone(longPhone);
//                if(null==user1){
//                    User user=new User();
//                    user.setUserTypeId("5b88e7a5dc8b5a1a84a96367");
//                    user.setName(xssfRow.getCell(2).getStringCellValue());
//                    user.setLongPhone(longPhone);
//                    user.setPassword(MD5Util.encode(user.getLongPhone()));
//                    user.setCollegeName("国教学院");
//                    String username=null;
//                    if(count<100)
//                        username="jg0"+count;
//                    else
//                        username="jg"+count;
//                    user.setUsername(username);
//                    user.setFlag(true);
//                    userRepository.insert(user);
//                    System.out.println(username);
//                    count++;
//                }
//            }
//            System.out.println(count);
//        }catch(FileNotFoundException e){
//            e.printStackTrace();
//        } catch(IOException e){
//            e.printStackTrace();
//        }
//    }
//    @Test
//        public void testImportSh()  {
//
//            try {
//    //            InputStream is=new FileInputStream("D:/Data.xls");
//    //         HSSFWorkbook tpWorkbook = new HSSFWorkbook(is);
//            List<User> users=userRepository.findForeignUser("sh");
//            HSSFWorkbook wb = new HSSFWorkbook();
//            HSSFSheet sheet = wb.createSheet("sheet1");
//            int count=0;
//            Iterator iterator=users.iterator();
//            while(iterator.hasNext()){
//                User user=(User)iterator.next();
//                HSSFRow row = sheet.createRow(count++);//创建表格行
//                HSSFCell cell = row.createCell(0);//根据表格行创建单元格
//                cell.setCellValue(user.getUsername());
//                HSSFCell cell1 = row.createCell(1);
//                cell1.setCellValue(user.getName());
//                HSSFCell cell2 = row.createCell(2);
//                 cell2.setCellValue(user.getLongPhone());
//            }
//                OutputStream f=new FileOutputStream("D:/sh.xls");
//                wb.write(f);
//                wb.close();
//                f.flush();
//                f.close();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//    @Test
//    public void testImportJg()  {
//
//        try {
//            //            InputStream is=new FileInputStream("D:/Data.xls");
//            //         HSSFWorkbook tpWorkbook = new HSSFWorkbook(is);
//            List<User> users=userRepository.findForeignUser("jg");
//            HSSFWorkbook wb = new HSSFWorkbook();
//            HSSFSheet sheet = wb.createSheet("sheet1");
//            int count=0;
//            Iterator iterator=users.iterator();
//            while(iterator.hasNext()){
//                User user=(User)iterator.next();
//                HSSFRow row = sheet.createRow(count++);//创建表格行
//                HSSFCell cell = row.createCell(0);//根据表格行创建单元格
//                cell.setCellValue(user.getUsername());
//                HSSFCell cell1 = row.createCell(1);
//                cell1.setCellValue(user.getName());
//                HSSFCell cell2 = row.createCell(2);
//                cell2.setCellValue(user.getLongPhone());
//            }
//            OutputStream f=new FileOutputStream("D:/jg.xls");
//            wb.write(f);
//            wb.close();
//            f.flush();
//            f.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }

//    @Test
//    public void testImportSh()  {
//        try {
//            List<SendPool>  sendPools=sendRepository.findAllByStatus(Constant.INFORMING);
//            HSSFWorkbook wb = new HSSFWorkbook();
//            HSSFSheet sheet = wb.createSheet("sheet1");
//            HSSFRow row1=sheet.createRow(0);
//            row1.createCell(0).setCellValue("调查名称");
//            row1.createCell(1).setCellValue("姓名");
//            row1.createCell(2).setCellValue("工号/学号");
//            row1.createCell(3).setCellValue("电话号码");
//            row1.createCell(5).setCellValue("类别");
//            row1.createCell(4).setCellValue("学院");
//            int count=1;
//            Iterator iterator=sendPools.iterator();
//            while(iterator.hasNext()){
//                User user=null;
//                SendPool sendPool=(SendPool) iterator.next();
//                user=userRepository.findByLongPhone(sendPool.getPhoneNumbr());
//                HSSFRow row = sheet.createRow(count++);//创建表格行
//                HSSFCell cell = row.createCell(0);//根据表格行创建单元格
//                cell.setCellValue(sendPool.getSurvey());
//                HSSFCell cell1 = row.createCell(1);
//                cell1.setCellValue(sendPool.getName());
//                HSSFCell cell2 = row.createCell(2);
//                cell2.setCellValue(sendPool.getNickName());
//                HSSFCell cell3 = row.createCell(3);
//                cell3.setCellValue(sendPool.getPhoneNumbr());
//                if(null!=user){
//                row.createCell(4).setCellValue(user.getCollegeName());
//                if(user.getUserTypeId().equals("5b88e7e8dc8b5a1a84a96368")){
//                    row.createCell(5).setCellValue("老师");
//                }else if(user.getUserTypeId().equals("5b88e7a5dc8b5a1a84a96367")){
//                    row.createCell(5).setCellValue("学生");
//                }else if(user.getUserTypeId().equals("5b88e7e8dc8b5a1a84a96370")){
//                    row.createCell(5).setCellValue("研究生");
//                }
//                }
//            }
//            OutputStream f=new FileOutputStream("D:/未填写名单.xls");
//            wb.write(f);
//            wb.close();
//            f.flush();
//            f.close();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
}
